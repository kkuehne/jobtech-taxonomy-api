# Design principles

The Jobtech Taxonomy database never deletes any data. Internally it just adds all changes to the database as an ever increasing event log. Concepts that shouldn't be used anymore gets flagged with a deprecated flag and will not be exposed via the API unless specifically asked for. The API allows it's consumers to choose what version of the database they want to retireve data from which allows the consumers to become eventually consistent, instead of forcing a big bang of the usage.    


### Example of changes endpoint 

```json
[  
  {
    "taxonomy/event-type": "CREATED",
    "taxonomy/new-concept": {
      "taxonomy/definition": "Big data analyst/Stordata-analytiker",
      "taxonomy/preferred-label": "Big data analyst/Stordata-analytiker",
      "taxonomy/id": "Ggze_QQS_jZi",
      "taxonomy/type": "occupation-name"
    }
    "taxonomy/version": 2
  },
  {
    "taxonomy/event-type": "DEPRECATED",
    "taxonomy/latest-version-of-concept": {
      "taxonomy/id": "3vQv_E4Q_wjK",
      "taxonomy/type": "occupation-name",
      "taxonomy/definition": "Trafikpedagog",
      "taxonomy/preferred-label": "Trafikpedagog"
    },
    "taxonomy/version": 2
  }
]

```


# Architechtural overview

![Architechtural overview](../pictures-for-md/taxonomy-architechure-1.svg)


# Systems overview

![Systems overview](../pictures-for-md/taxonomy-architechure-2.svg)
