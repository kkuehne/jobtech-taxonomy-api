(ns jobtech-taxonomy-api.handler
  (:require
   [expound.alpha :as expound]
   [jobtech-taxonomy-api.env :refer [defaults]]
   [jobtech-taxonomy-api.middleware.formats :as formats]
   [jobtech-taxonomy-api.middleware.misc :refer [log-request trim-request-params]]
   [jobtech-taxonomy-api.routes.services :refer [service-routes]]
   [mount.core :as mount]
   [reitit.coercion.spec :as spec-coercion]
   [reitit.dev.pretty :as pretty]
   [reitit.ring :as ring]
   [reitit.ring.coercion :as coercion]
   [reitit.ring.middleware.dev :as dev]
   [reitit.ring.middleware.exception :as exception]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.parameters :as parameters]
   [reitit.ring.spec :as spec]
   [reitit.swagger :as swagger]
   [spec-tools.spell :as spell]))

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

(defn handler [exception request]
  {:status 500
   :description "Internal Server Error"
   :body {:message (str exception)}})

(mount/defstate app
  :start
  (ring/ring-handler
   (ring/router
    (service-routes)
    {:data {:reitit.middleware/transform dev/print-request-diffs ;; pretty diffs
            :validate spec/validate ;; enable spec validation for route data
            :reitit.spec/wrap spell/closed ;; strict top-level validation

            :exception pretty/exception
            :coercion reitit.coercion.spec/coercion
            :muuntaja formats/instance
            :middleware [;; request logging
                         log-request
                         ;; swagger feature
                         swagger/swagger-feature
                         ;; query-params & form-params
                         parameters/parameters-middleware
                         ;; trim input params
                         trim-request-params
                         ;; content-negotiation
                         muuntaja/format-negotiate-middleware
                         ;; encoding response body
                         muuntaja/format-response-middleware
                         ;; exception handling
                         (exception/create-exception-middleware
                          (merge
                           exception/default-handlers
                           {::exception/default handler

                            ;; print stack-traces for all exceptions except for invalid user input
                            ::exception/wrap (fn [fn-handler e request]
                                               (when-not (= :reitit.coercion/request-coercion (:type (ex-data e)))
                                                 (expound/printer (:problems (ex-data e))))
                                               (fn-handler e request))}))

                         ;; decoding request body
                         muuntaja/format-request-middleware
                         ;; coercing response bodys
                         coercion/coerce-response-middleware
                         ;; coercing request parameters
                         coercion/coerce-request-middleware
                         ;; multipart
                         multipart/multipart-middleware]}})
   (ring/routes
    (ring/create-resource-handler
     {:path "/"})

    (ring/create-default-handler))))
