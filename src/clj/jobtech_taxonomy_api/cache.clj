(ns jobtech-taxonomy-api.cache
  (:import [com.github.benmanes.caffeine.cache Caffeine]
           [java.util.function Function]))

(defn make
  "Create a cache that wraps referentially transparent load-fn

  Returns a function that caches invocation results by arguments (optionally
  transformed with :key-fn function)

  Optional kv-args:
    :key-fn          fn with the same arity as load-fn that converts load-fn
                     args to cache key, defaults to clojure.core/vector
    :maximum-size    number, maximum cache size"
  [load-fn & {:keys [key-fn maximum-size]
              :or {key-fn vector}}]
  (let [c (-> (Caffeine/newBuilder)
              (cond-> maximum-size (.maximumSize maximum-size))
              (.build))]
    (fn get-from-cache [& args]
      (.get c (apply key-fn args) (reify Function
                                    (apply [_ _]
                                      (apply load-fn args)))))))
