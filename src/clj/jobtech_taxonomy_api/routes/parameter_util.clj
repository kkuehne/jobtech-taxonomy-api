(ns jobtech-taxonomy-api.routes.parameter-util
  (:require [jobtech-taxonomy-api.authentication-service :as keymanager]
            [jobtech-taxonomy-common.relation :as relation]
            [spec-tools.data-spec :as ds]
            [taxonomy :as taxonomy]))

(defn relation-types []
  (set (keys relation/relations)))

(defn legacy-query-relation-types []
  (conj (relation-types) "substitutability-to" "substitutability-from"))

(def common-parameters
  {:user-id [(ds/opt :user-id) (taxonomy/par string? "User id")]
   :id [(ds/opt :id) (taxonomy/par string? "Concept id")]
   :type [(ds/opt :type) (taxonomy/par string? "Concept type")]
   :definition [(ds/opt :definition) (taxonomy/par string? "Definition")]
   :preferred-label [(ds/opt :preferred-label) (taxonomy/par string? "Preferred label")]
   :deprecated [(ds/opt :deprecated) (taxonomy/par boolean? "Restrict to deprecation state" :swagger/deprecated true)]
   :include-deprecated [(ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")]
   :comment [(ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")]
   :quality-level [(ds/opt :quality-level) :taxonomy/quality-level]

   ;; Warning the relation is not the same for all endpoints. use relation for concepts, use relation type for relation endpoints that doens care about direction

   :relation [(ds/opt :relation) (-> (taxonomy/par (legacy-query-relation-types)
                                                   "Relation type. If specified, related-ids is required. \"substitutability-to\" and \"substitutability-from\" are deprecated, please use \"substitutability\" and \"substituted-by\" respectively")
                                     (assoc :form (legacy-query-relation-types)))]
   :relation-type [(ds/opt :relation-type) (-> (taxonomy/par (relation-types) "Relation type")
                                               (assoc :form (relation-types)))]

   :related-ids [(ds/opt :related-ids) (taxonomy/par string? "OR-restrict to these relation IDs (white space separated list)")]
   :offset [(ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)")]
   :limit [(ds/opt :limit) (taxonomy/par int? "Return list limit")]
   :version [(ds/opt :version) taxonomy/version-param]
   :concept-1 [(ds/opt :concept-1) (taxonomy/par string? "ID of source concept")]
   :concept-2 [(ds/opt :concept-2) (taxonomy/par string? "ID of target concept")]
   :substitutability-percentage [(ds/opt :substitutability-percentage) (taxonomy/par pos-int? "You only need this one if the relation is substitutability")]

   :alternative-labels [(ds/opt :alternative-labels) (taxonomy/par string? "Alternative labels. Pipe separated")]

   :hidden-labels [(ds/opt :hidden-labels) (taxonomy/par string? "Hidden labels. Pipe separated")]

   :alternative-label [(ds/opt :alternative-label) (taxonomy/par string? "Alternative label to remove")]

   :hidden-label [(ds/opt :hidden-label) (taxonomy/par string? "Hidden label to remove")]})

(defn build-parameter-map [params]
  (reduce (fn [acc param]
            (let [[k v] (get common-parameters param)]
              (assoc acc k v))) {} params))

(defn get-param [k]
  (get-in common-parameters [k 1]))

(defn get-user-id-from-request [request]
  (keymanager/get-user-id-from-api-key (get (:headers request) "api-key")))

(defn get-query-from-request [request]
  (:query (:parameters request)))

(def relation-type
  (-> (taxonomy/par (relation-types) "Relation type")
      (assoc :form (relation-types))))

(def concept-source
  (taxonomy/par string? "ID of source concept"))

(def concept-target
  (taxonomy/par string? "ID of target concept"))

(def relation-description
  (taxonomy/par string? "Description of the relation"))

(def daynote-comment
  (taxonomy/par string? "Daynote comment for this action"))

(def substitutability-percentage
  (taxonomy/par pos-int? "You only need this one if the relation is substitutability"))
