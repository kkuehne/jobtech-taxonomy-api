(ns jobtech-taxonomy-api.routes.services
  (:refer-clojure :exclude [type])
  (:require [buddy.auth.http :as http]
            [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.concept-types :as concept-types]
            [jobtech-taxonomy-api.db.core :as core]
            [jobtech-taxonomy-api.db.daynotes :as daynotes]
            [jobtech-taxonomy-api.db.eures :as eures]
            [jobtech-taxonomy-api.db.events :as events]
            [jobtech-taxonomy-api.db.graph :as graph]
            [jobtech-taxonomy-api.db.graphql :as graphql]
            [jobtech-taxonomy-api.db.graphviz :as graphviz]
            [jobtech-taxonomy-api.db.legacy :as legacy]
            [jobtech-taxonomy-api.db.postalcodes :as postalcodes]
            [jobtech-taxonomy-api.db.search :as search]
            [jobtech-taxonomy-api.db.versions :as v]
            [jobtech-taxonomy-api.db.webhooks :as webhooks-db]
            [jobtech-taxonomy-api.middleware :as middleware]
            [jobtech-taxonomy-api.middleware.cors :as cors]
            [jobtech-taxonomy-api.middleware.formats :as formats]
            [jobtech-taxonomy-api.routes.parameter-util :as pu]
            [jobtech-taxonomy-api.routes.swagger3 :as swagger3]
            [jobtech-taxonomy-api.webhooks :as webhooks]
            [muuntaja.core :as m]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [ring.util.http-response :as resp]
            [ring.util.response :as response]
            [spec-tools.core :as st]
            [spec-tools.data-spec :as ds]
            [taxonomy :as taxonomy]))

;; Status:
;;   - fixa replaced-by-modell i /changes, /replaced-by-changes, /search, /private/concept


(defn- authorized-private?
  [handler]
  (fn [request]
    (if (middleware/authenticate-admin (http/-get-header request "api-key"))
      (handler request)
      (resp/unauthorized (taxonomy/map->nsmap {:error "Not authorized"})))))

(defn private-query-params
  "Creates middleware that ensures admin authentication for certain query values

  Expects a map from query parameter to a predicate that should be satisfied to trigger
  admin authentication check, for example:
  ```
  {:sudo true?
   :version #{:next}
   :raw-sql-query any?}
  ```
  This param->pred map will require admin rights if either:
  - `:sudo` parameter is `true`
  - `:version` parameter is `:next`
  - `:raw-sql-query` is present"
  [param->pred]
  (let [needs-admin-auth? (apply some-fn
                                 (map (fn [[k pred]]
                                        (fn [query-params]
                                          (let [v (get query-params k ::not-found)]
                                            (and (not= v ::not-found) (pred v)))))
                                      param->pred))]
    (fn [handler]
      (fn [request]
        (if (and (needs-admin-auth? (get-in request [:parameters :query]))
                 (not (middleware/authenticate-admin (http/-get-header request "api-key"))))
          (resp/unauthorized (taxonomy/map->nsmap {:error "Not authorized"}))
          (handler request))))))

(defn catch-errors [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception e
        (log/error e "Error while performing request")
        (merge taxonomy/response500
               {:body {::taxonomy/error (.getMessage e)}})))))

(def graphql-response-handler
  {:responses (taxonomy/response200 any?)
   :handler (fn [{{{:keys [query variables operationName]} :query} :parameters :as request}]
              (let [api-key (http/-get-header request "api-key")
                    resp (graphql/execute query
                                          (some->> variables (m/decode formats/instance "application/json"))
                                          operationName
                                          api-key)
                    msg (:message (first (:errors resp)))
                    [status body]
                    (cond
                      (and (not (nil? msg)) (.contains msg "Not authorized"))
                      [401 (taxonomy/map->nsmap {:error msg})]
                      (contains? resp :errors)
                      [400 (taxonomy/map->nsmap {:error msg})]
                      :else
                      [200 resp])]
                {:status status
                 :body body}))})

(def swagger-header
  "Jobtech Taxonomy Endpoints.\n
  Alternatively, explore the Taxonomy data with the
  <a href=\"https://atlas.jobtechdev.se\">JobTech Taxonomy Atlas</a>")

(defn service-routes []
  ["/v1/taxonomy"
   {:coercion (swagger3/wrap-coercion spec-coercion/coercion)
    :muuntaja formats/instance
    :middleware [cors/cors catch-errors]
    :responses (merge taxonomy/response401 taxonomy/response500)
    :swagger {:id ::api
              ;; openapi 3.0 disallows both "swagger" and "openapi" keys in swagger.json
              ;; having non-null values, but `swagger/create-swagger-handler` always sets "swagger" key
              ;; to 2.0. Setting `:swagger` key to `nil` here overrides that key, which allows us
              ;; to satisfy openapi spec (at least in the ui handler?)
              :swagger nil
              :openapi "3.0.0"
              :info {:version "0.10.0"
                     :title "Jobtech Taxonomy"
                     :description swagger-header}
              :components {:securitySchemes {:api-key {:type "apiKey" :name "api-key" :in "header"}}}
              :security [{:api-key []}]}}

   ["" {:no-doc true}
    ["/swagger.json"
     {:get {:middleware [swagger3/swagger2->openapi3]
            :handler (swagger/create-swagger-handler)}}]

    ["/swagger-ui*"
     {:get {:handler (swagger-ui/create-swagger-ui-handler
                      {:url "/v1/taxonomy/swagger.json"

                       :config {:operationsSorter "alpha"}})}}]

    ["/graphiql"
     ["" {:get {:handler (fn [_] (response/url-response (io/resource "public/graphiql.html")))}}]
     [".json" {:get {:handler (constantly (resp/ok {:url "/v1/taxonomy/graphql"}))}}]]]

   ["/graphql"
    {:swagger {:tags ["GraphQL"]}
     :summary "GraphQL endpoint for powerful graph queries"
     :description "You should explore this API in [this GraphQL-specific explorer](/v1/taxonomy/graphiql)"
     :parameters {:query {:query (taxonomy/par string? "query string")
                          (ds/opt :variables) (taxonomy/par string? "json string with query variables")
                          (ds/opt :operationName) (taxonomy/par string? "optional query name that is used when there are multiple query definitions in a single query string")}}
     :get graphql-response-handler
     :post graphql-response-handler}]

   ["/graphviz"
    {:swagger {:tags ["Graphviz"]}}
    ["/types.svg"
     {:parameters {:query {(ds/opt :version) taxonomy/version-param}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses {200 {:body string?
                             :content {"image/svg+xml" {}}
                             :description "OK"}}
            :handler (fn [{{{:keys [version]
                             :or {version :latest}} :query} :parameters}]
                       {:status 200
                        :headers {"Content-Type" "image/svg+xml"}
                        :body (graphviz/types-svg {:version version})})}}]]

   ["/main"
    {:swagger {:tags ["Main"]}}

    ["/versions"
     {:summary "Show the available versions."
      :get {:responses (taxonomy/response200 taxonomy/versions-spec)
            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body (map taxonomy/map->nsmap (v/get-all-versions))})}}]

    ["/concepts"
     {:summary "Get concepts. Supply at least one search parameter."
      :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept"),
                           (ds/opt :preferred-label) (taxonomy/par string? "Textual name of concept"),
                           (ds/opt :type) (st/spec {:name "types" :spec string? :description "Restrict to concept type"})
                           (ds/opt :deprecated) (taxonomy/par boolean? "Restrict to deprecation state" :swagger/deprecated true),
                           (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated values"),
                           (ds/opt :relation) (pu/get-param :relation),
                           (ds/opt :related-ids) (taxonomy/par string? "OR-restrict to these relation IDs (white space separated list). If specified, relation is required"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit"),
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-legacy-information) (taxonomy/par boolean? "This parameter will be removed. Include information related to Arbetsförmedlingen's old internal taxonomy"
                                                                              :swagger/deprecated true)}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses (taxonomy/response200 taxonomy/concepts-spec)
            :handler (fn [{{{:keys [id preferred-label type include-deprecated deprecated
                                    relation related-ids offset limit version
                                    include-legacy-information]
                             :or {version :latest relation "related"
                                  include-legacy-information false}} :query} :parameters}]
                       {:status 200
                        :body (vec (map taxonomy/map->nsmap (concepts/find-concepts
                                                             {:id id
                                                              :preferred-label preferred-label
                                                              :type (when type (str/split type #" "))
                                                              :include-deprecated include-deprecated
                                                              :deprecated deprecated
                                                              :relation relation
                                                              :related-ids (when related-ids (str/split related-ids #" "))
                                                              :offset offset
                                                              :limit limit
                                                              :version version
                                                              :include-legacy-information include-legacy-information})))})}}]

    ["/concept/changes"
     {:summary "Show changes to concepts as a stream of events."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occured after this version was published."),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occured before this version was published and during this version. (default: latest version)"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}
      :get {:responses (taxonomy/response200 taxonomy/concept-changes-spec)
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       {:status 200
                        :body (let [events (daynotes/get-concept-changes-with-pagination-public after-version to-version-inclusive offset limit)]
                                (vec (map taxonomy/map->nsmap events)))})}}]

    ["/graph"
     {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
      :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                           :source-concept-type (taxonomy/par string? "Source nodes concept type")
                           :target-concept-type (taxonomy/par string? "Target nodes concept type")
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)")
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses (taxonomy/response200 taxonomy/graph-spec)
            :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                             :or {version :latest}} :query} :parameters}]
                       {:status 200
                        :body (graph/fetch-graph edge-relation-type source-concept-type target-concept-type
                                                 include-deprecated offset limit version)})}}]

    ["/replaced-by-changes"
     {:summary "Show the history of concepts being replaced after a given version."
      :parameters {:query {:after-version (taxonomy/par int? "After what taxonomy version did the change occur"),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occured before this version was published and during this version. (default: latest version)")}}
      :get {:responses (taxonomy/response200 taxonomy/replaced-by-changes-spec)
            :handler (fn [{{{:keys [after-version to-version-inclusive]} :query} :parameters}]
                       {:status 200
                        :body (vec (map taxonomy/map->nsmap (events/get-deprecated-concepts-replaced-by-from-version after-version to-version-inclusive)))})}}]

    ["/concept/types"
     {:summary "Return a list of all taxonomy types."
      :parameters {:query {(ds/opt :version) taxonomy/version-param}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses (taxonomy/response200 taxonomy/concept-types-spec)
            :handler (fn [{{{:keys [version]
                             :or {version :latest}} :query} :parameters}]
                       {:status 200
                        :body (vec (core/get-all-taxonomy-types version))})}}]

    ["/relation/types"
     {:summary "Relation types. Type 'narrower' is not listed here, as it exists as the inverse of 'broader'."
      :get {:responses (taxonomy/response200 taxonomy/relation-types-spec)
            :handler (fn [{{{:keys []} :query} :parameters}]
                       {:status 200
                        :body (vec (core/get-relation-types))})}}]

    ["/relation/changes"
     {:summary "Show changes to the relations as a stream of events."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changed relations that occured after this version was published."),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changed relations that occured before this version was published and during this version. (default: latest version)"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}
      :get {:responses (taxonomy/response200 [any?])
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       {:status 200
                        :body (let [events (doall (daynotes/get-relation-changes-with-pagination-public after-version to-version-inclusive offset limit))]
                                (vec (map taxonomy/map->nsmap events)))})}}]]

   ["/specific"
    {:swagger {:tags ["Specific Types"]
               :description "Exposes concept with detailed information such as codes from external standards."}}
    (map
     #(taxonomy/create-detailed-endpoint % concepts/find-concepts)
     taxonomy/detailed-endpoint-configs)]

   ["/suggesters"
    {:swagger {:tags ["Suggesters"]}}

    ["/autocomplete"
     {:summary "Autocomplete from query string"
      :description "Help end-users to find relevant concepts from the taxonomy"
      :parameters {:query {:query-string (taxonomy/par string? "String to search for"),
                           (ds/opt :type) (taxonomy/par string? "Type to search for"),
                           (ds/opt :relation) (pu/get-param :relation),
                           (ds/opt :related-ids) (taxonomy/par string? "List of related IDs to search for"),
                           (ds/opt :search-type) (taxonomy/par #{"contains" "starts-with"} "This parameter is ignored" :swagger/deprecated true)
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")
                           (ds/opt :version) taxonomy/version-param}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses (taxonomy/response200 taxonomy/autocomplete-spec)
            :handler (fn [{{:keys [query]} :parameters}]
                       {:status 200
                        :body (vec (map taxonomy/map->nsmap
                                        (search/get-concepts-by-search
                                         (-> query
                                             (update :version #(or % :latest))
                                             (update :relation #(or % "related"))
                                             (update :type #(some-> % (str/split #" ")))
                                             (update :related-ids #(some-> % (str/split #" ")))))))})}}]]

   ["/private"
    {:swagger {:tags ["Private"]}
     :middleware [authorized-private?]}

    ["/concept/changes"
     {:summary "Show changes to the taxonomy as a stream of events. Include unpublished changes."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occured after this version was published."),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occured before this version was published and during this version. (default: latest version)")
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}
      :get {:responses (taxonomy/response200 taxonomy/concept-changes-spec)
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       {:status 200
                        :body (let [events (daynotes/get-concept-changes-with-pagination-private after-version to-version-inclusive offset limit)]
                                (vec (map taxonomy/map->nsmap events)))})}}]

    ["/delete-concept"
     {:summary "Retract the concept with the given ID."
      :parameters {:query {:id (taxonomy/par string? "ID of concept")
                           (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
      :delete {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
               :handler (fn [request]
                          (let [{:keys [id comment]} (pu/get-query-from-request request)
                                user-id (pu/get-user-id-from-request request)]
                            (if (core/retract-concept user-id id comment)
                              {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                              {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

    ["/concept"
     {:summary "Assert a new concept."
      :parameters {:query (pu/build-parameter-map [:type :definition :preferred-label :comment :quality-level :alternative-labels :hidden-labels])}
      :post {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
             :handler concepts/assert-concept-handler}}]

    ["/remove-alternative-label"
     {:summary "Remove alternative label from concept."
      :parameters {:query
                   (pu/build-parameter-map [:id :comment :alternative-label])}

      :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
              :handler concepts/handle-remove-alternative-label}}]

    ["/remove-hidden-label"
     {:summary "Remove hidden label from concept."
      :parameters {:query
                   (pu/build-parameter-map [:id :comment :hidden-label])}

      :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
              :handler concepts/handle-remove-hidden-label}}]

    ["/concept-types"
     {:summary "Assert concept types and their localisation"
      :parameters {:query {:concept-type (taxonomy/par string? "Concept type")
                           (ds/opt :label-sv) (taxonomy/par string? "Label in Swedish")
                           (ds/opt :label-en) (taxonomy/par string? "Label in English")}}
      :post {:responses (taxonomy/response200 any?)
             :handler (fn [req]
                        {:status 200
                         :body (concept-types/assert-type
                                (-> req
                                    :parameters
                                    :query
                                    (assoc :user-id (pu/get-user-id-from-request req))))})}}]

    ["/concepts"
     {:summary "Get concepts. Supply at least one search parameter."
      :parameters {:query
                   (pu/build-parameter-map
                    [:id :preferred-label :type :include-deprecated :deprecated
                     :relation :related-ids :offset :limit :version])}
      :get {:responses (taxonomy/response200 taxonomy/concepts-spec-private)
            :handler (fn [{{{:keys [id preferred-label type include-deprecated deprecated
                                    relation related-ids offset limit version]
                             :or {version :next relation "related"}} :query} :parameters}]
                       {:status 200
                        :body (vec (map taxonomy/map->nsmap (concepts/find-concepts
                                                             {:id id
                                                              :preferred-label preferred-label
                                                              :type (when type (str/split type #" "))
                                                              :include-deprecated include-deprecated
                                                              :deprecated deprecated
                                                              :relation relation
                                                              :related-ids (when related-ids (str/split related-ids #" "))
                                                              :offset offset
                                                              :limit limit
                                                              :version version})))})}}]

    ["/accumulate-concept"
     {:summary "Accumulate data on an existing concept."
      :parameters {:query
                   (pu/build-parameter-map [:id :type :definition :preferred-label :comment :quality-level :deprecated :alternative-labels :hidden-labels])}
      :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
              :handler (fn [request]
                         (let [query (pu/get-query-from-request request)
                               user-id (pu/get-user-id-from-request request)
                               result (concepts/accumulate-concept user-id query)]
                           (if result
                             {:status 200 :body result}
                             {:status 409 :body (taxonomy/map->nsmap {:error "Can't update concept since it is in conflict with existing concept."})})))}}]

    ["/relation"
     {:summary "Assert a new relation."
      :parameters {:query (concepts/assert-relation-query-params)}
      :post {:responses (merge (taxonomy/response200 taxonomy/msg-spec) taxonomy/response409)
             :handler concepts/assert-relation-handler}}]

    ["/accumulate-relation"
     {:summary "Accumulate data on an existing relation."
      :parameters {:query {:concept-1 pu/concept-source
                           :concept-2 pu/concept-target
                           :relation-type pu/relation-type
                           :comment pu/daynote-comment
                           (ds/opt :description) pu/relation-description
                           (ds/opt :substitutability-percentage) pu/substitutability-percentage}}
      :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
              :handler (fn [request]
                         (let [query (pu/get-query-from-request request)
                               user-id (pu/get-user-id-from-request request)
                               result (concepts/accumulate-relation user-id query)]
                           (if result
                             {:status 200 :body result}
                             (let [msg "Can't update relation since it is in conflict with existing relation."]
                               {:status 409 :body (taxonomy/map->nsmap {:error msg})}))))}}]

    ["/delete-relation"
     {:summary "Retract a relation."
      :parameters {:query (concepts/delete-relation-query-params)}
      :delete {:responses (merge (taxonomy/response200 taxonomy/msg-spec) taxonomy/response409)
               :handler concepts/delete-relation-handler}}]

    ["/relation/changes"
     {:summary "Show changes to the relations as a stream of events."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changed relations that occured after this version was published."),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changed relations that occured before this version was published and during this version. (default: latest version)"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}
      :get {:responses (taxonomy/response200 [any?])
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       {:status 200
                        :body (let [events (doall (daynotes/get-relation-changes-with-pagination-private after-version to-version-inclusive offset limit))]
                                (vec (map taxonomy/map->nsmap events)))})}}]

    ["/graph"
     {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
      :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                           :source-concept-type (taxonomy/par string? "Source nodes concept type")
                           :target-concept-type (taxonomy/par string? "Target nodes concept type")
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)")
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
      :get {:responses (taxonomy/response200 taxonomy/graph-spec)
            :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                             :or {version :next}} :query} :parameters}]
                       {:status 200
                        :body (graph/fetch-graph edge-relation-type source-concept-type target-concept-type
                                                 include-deprecated offset limit version)})}}]

    ["/replace-concept"
     {:summary "Replace old concept with a new concept."
      :parameters {:query {:old-concept-id (taxonomy/par string? "Old concept ID"),
                           :new-concept-id (taxonomy/par string? "New concept ID")
                           (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
      :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
             :handler (fn [{{{:keys [old-concept-id new-concept-id comment]} :query} :parameters :as request}]
                        (let [user-id (pu/get-user-id-from-request request)]
                          (if (core/replace-deprecated-concept user-id old-concept-id new-concept-id comment)
                            {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                            {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

    ["/unreplace-concept"
     {:summary "Undo a replacement of the old concept with a new concept, while keeping it deprecated"
      :parameters {:query {:old-concept-id (taxonomy/par string? "Old concept ID"),
                           :new-concept-id (taxonomy/par string? "New concept ID")
                           (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
      :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
             :handler (fn [{{{:keys [old-concept-id new-concept-id comment]} :query} :parameters :as request}]
                        (let [user-id (pu/get-user-id-from-request request)]
                          (if (core/unreplace-deprecated-concept user-id old-concept-id new-concept-id comment)
                            {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                            {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

    ["/versions"
     {:summary "Creates a new version tag in the database."
      :parameters {:query {:new-version-id (taxonomy/par int? "New version ID")
                           (ds/opt :new-version-timestamp) (taxonomy/par inst? "New version timestamp (default: now)")}}
      :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response406)
             :handler (fn [{{{:keys [new-version-id new-version-timestamp]} :query} :parameters}]
                        (case (v/create-new-version new-version-id new-version-timestamp)
                          ::v/incorrect-new-version
                          {:status 406
                           :body (taxonomy/map->nsmap {:error (str new-version-id " is not the next valid version id!")})}

                          ::v/new-version-before-release
                          {:status 406
                           :body (taxonomy/map->nsmap {:error (str new-version-timestamp " is before the latest release!")})}

                          (let [clients (webhooks/get-client-list-from-db!)]
                            (webhooks/notify-all! clients new-version-id)
                            {:status 200
                             :body (taxonomy/map->nsmap {:message "A new version of the Taxonomy was created."})})))}}]

    ["/concept/automatic-daynotes/"
     {:get {:summary "Fetches concept day notes (optionally restricted to a single concept id)"
            :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                 (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this point in time (inclusive)")
                                 (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this point in time (exclusive)")}}
            :responses (merge (taxonomy/response200 any?) taxonomy/response404)
            :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                       {:status 200
                        :body (daynotes/get-for-concept id from-timestamp to-timestamp nil nil true)})}
      :post {:summary "Create a concept daynote"
             :parameters {:query {:id (taxonomy/par string? "ID of concept")
                                  :comment (taxonomy/par string? "Comment")}}
             :responses (taxonomy/response200 taxonomy/ok-spec)
             :handler (fn [{{{:keys [id comment]} :query} :parameters :as request}]
                        (daynotes/create-for-concept id (pu/get-user-id-from-request request) comment)
                        {:status 200
                         :body (taxonomy/map->nsmap {:message "ok"})})}}]

    ["/relation/automatic-daynotes/"
     {:get {:summary "Fetches relation day notes (optionally restricted to relations of a particular concept id)"
            :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                 (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this date (inclusive)")
                                 (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this date (exclusive)")}}
            :responses (merge (taxonomy/response200 [any?]) taxonomy/response404)
            :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                       {:status 200
                        :body (daynotes/get-for-relation id from-timestamp to-timestamp nil nil true)})}
      :post {:summary "Create a relation daynote"
             :parameters {:query {:concept-1 (taxonomy/par string? "ID of source concept")
                                  :concept-2 (taxonomy/par string? "ID of target concept")
                                  :relation-type (taxonomy/par #{"broader" "related" "substitutability"} "Relation type")
                                  :comment (taxonomy/par string? "Comment")}}
             :responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
             :handler (fn [{{{:keys [concept-1 concept-2 relation-type comment]} :query} :parameters :as request}]
                        (if (daynotes/create-for-relation (pu/get-user-id-from-request request)
                                                          comment
                                                          concept-1
                                                          concept-2
                                                          relation-type)
                          {:status 200
                           :body (taxonomy/map->nsmap {:message "ok"})}
                          {:status 404
                           :body (taxonomy/map->nsmap {:error "relation not found"})}))}}]]

   ["/webhooks"
    {:swagger {:tags ["Webhooks"]
               :deprecated true}
     :middleware [authorized-private?]}

    ["/register"
     {:summary ""
      :parameters {:query {:api-key (taxonomy/par string? "A unique API-key used as identifier.")
                           :callback-url (taxonomy/par string? "A URL to a webhook callback listener.")}}
      :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response406)
             :handler (fn [{{{:keys [api-key callback-url]} :query} :parameters}]
                        (let [result (webhooks-db/update-webhooks api-key callback-url)]
                          (if result
                            {:status 200 :body (taxonomy/map->nsmap
                                                {:message (format "The webhook callback was registered.")})}
                            {:status 406 :body (taxonomy/map->nsmap {:error (str "Could not create webhook callback with values " api-key " " callback-url)})})))}}]]

   ["/legacy"
    {:swagger {:tags ["Legacy"]}}

    ["/convert-matching-component-id-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids from the old matching component to new Taxonomy ids. The old matching component used SSYK codes, municipality codes and region codes as ids so this endpoint implements the same bad behaviour."
      :parameters {:query {:matching-component-id (taxonomy/par string? "An id form the old matching component.")
                           :type legacy/concepts-types}}
      :get {:responses legacy/responses
            :handler legacy/convert-matching-component-id-to-new-id-handler}}]

    ["/convert-old-id-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids to new Taxonomy ids."
      :parameters {:query {:legacy-id (taxonomy/par string? "A legacy id.")
                           :type legacy/concepts-types}}
      :get {:responses legacy/responses
            :handler legacy/convert-old-id-to-new-id-handler}}]

    ["/convert-new-id-to-old-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between new ids to ikd Taxonomy ids. "
      :parameters {:query {:id (taxonomy/par string? "A taxonomy id")}}
      :get {:responses legacy/responses
            :handler legacy/convert-new-id-to-old-id-handler}}]

    ["/convert-ssyk-2012-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between SSYK 2012 code to new Taxonomy ids. SSYK 2012 is also known as occupation group / yrkesgrupp in older applications."
      :parameters {:query {:code (taxonomy/par string? "The SSYK 2012 code.")}}
      :get {:responses legacy/responses
            :handler legacy/convert-ssyk-2012-code-to-new-id-handler}}]

    ["/convert-municipality-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between municipality code to new Taxonomy ids. "
      :parameters {:query {:code (taxonomy/par string? "The municipality code.")}}
      :get {:responses legacy/responses
            :handler legacy/convert-municipality-code-to-new-id-handler}}]

    ["/convert-swedish-region-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between Swedish län code to new Taxonomy ids. "
      :parameters {:query {:code (taxonomy/par string? "The region code.")}}
      :get {:responses legacy/responses
            :handler legacy/convert-swedish-region-code-to-new-id}}]

    ["/get-occupation-name-with-relations"
     {:summary "This endpoint is only for legacy applications wanting a translation dump with ids for occupation names and their relations. "
      :get {:responses (taxonomy/response200 taxonomy/legacy-mapping-concepts)
            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body (map taxonomy/map->nsmap (legacy/get-occupation-name-with-relations))})}}]

    ["/get-old-legacy-education-levels"
     {:summary "This endpoint is only for legacy applications wanting old deprecated legacy education levels. "
      :get {:responses (taxonomy/response200 [{:preferred_label string?,
                                               :id string?,
                                               :deprecated_legacy_id_2 string?,
                                               :definition string?,
                                               :deprecated_legacy_id string?,
                                               :sun_education_level_code_2000 string?}])

            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body  legacy/old-deprecated-legacy-education-levels})}}]

    ["/postalcodes-municipality"
     {:summary "This endpoint is only for legacy applications using postalcodes. "
      :get {:responses  (taxonomy/response200 [{:postal_code string?
                                                :locality_preferred_label string?
                                                :municipality_id string?
                                                :national_nuts_level_3_code_2019 string?
                                                :municipality_preferred_label string?
                                                :municipality_deprecated_legacy_id string?
                                                :lau_2_code_2015 string?
                                                :region_id string?
                                                :region_term string?
                                                :region_deprecated_legacy_id string?}])

            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body (postalcodes/fetch-postalcodes)})}}]]

   ["/eures"
    {:swagger {:tags ["Eures"]}}

    ["/convert-occupation-name-concept-id-to-eures-esco-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between occupation names concept id to eures esco id. "
      :parameters {:query {:id (taxonomy/par string? "ID of concept")
                           (ds/opt :version) taxonomy/version-param}}
      :get {:responses eures/responses
            :handler eures/handler}}]]

   ["/status"
    {:swagger {:tags ["Status"]}}

    ["/ready"
     {:summary "A readiness health check for the database"
      :get {:responses (taxonomy/response200 any?)
            :handler (fn [_]
                       (let [res (v/get-all-versions)]
                         {:status 200
                          :body {:available (some? res)}}))}}]

    ["/health"
     {:summary "A health check for the API server that returns immediately"
      :get {:responses (taxonomy/response200 any?)
            :handler (fn [_]
                       {:status 200
                        :body {:available true}})}}]]])
