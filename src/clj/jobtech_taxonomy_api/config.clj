(ns jobtech-taxonomy-api.config
  (:require [cprop.core :refer [load-config]]
            [cprop.source :as source]
            [mount.core :refer [args defstate]]))

;; Set the database backend; :datomic or :datahike
(def db-backend
  (if-some [backend (System/getenv "DATABASE_BACKEND")]
    (keyword backend)
    :datomic))

;; Disable the query cache for some performance tests
(def enable-query-cache
  (if-some [caching (System/getenv "ENABLE_QUERY_CACHE")]
    (Boolean/valueOf caching)
    true))

;; The current number of deployed pods
(def nbr-pods 3)

;; For datomic this is 64 at the moment
(def nbr-db-connections (quot 64 nbr-pods))

(def test-prefix "/test")

(defn test-env!? []
  (= (System/getenv "DATOMIC_CFG__REGION") "eu-west-1"))

(defn make-config []
  (let [base-list [(args)
                   (source/from-system-props)
                   (source/from-env)]]
    (load-config :merge base-list)))

(defstate env
  :start
  (make-config))
