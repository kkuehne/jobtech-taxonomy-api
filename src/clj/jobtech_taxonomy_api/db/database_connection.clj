(ns jobtech-taxonomy-api.db.database-connection
  (:require [clojure.string :as str]
            [datahike.api :as datahike]
            [datomic.client.api :as datomic]
            [datomic.client.impl.shared :as datomic-impl]
            [jobtech-taxonomy-api.config :refer [db-backend enable-query-cache env]]
            [jobtech-taxonomy-api.cache :as cache]
            [mount.core :refer [defstate]]
            [sendandi.api :as datahike-sendandi]
            [wanderung.core :as wanderung])
  (:import [datahike.db DB AsOfDB SinceDB HistoricalDB FilteredDB]))

(defn- retry-helper [delay-ns nbr f & params]
  (try
    (apply f params)
    (catch Exception e
      (let [msg (ex-message e)]
        (cond
          (str/starts-with? msg "jobtech-taxonomy-api-retry: ")
          (throw e)
          (> nbr 5)
          (let [new-ex (Exception. (str "jobtech-taxonomy-api-retry: " msg))]
            (throw (doto new-ex (.setStackTrace (.getStackTrace e)))))
          :else
          (do
            (Thread/sleep (* nbr delay-ns))
            (println (str "Retry " nbr " caught exception: " msg))
            (apply retry-helper delay-ns (inc nbr) f params)))))))

;; Retry a function that throws an exception 5 times
(defn retry [delay-ns f & params]
  (apply retry-helper delay-ns 1 f params))

(defn backend-specific-fn [& backend-fns]
  (let [backend-map (apply hash-map backend-fns)]
    (fn [& args]
      (if-let [f (get backend-map db-backend)]
        (apply f args)
        (throw (ex-info (str "Missing backend fn for " db-backend) backend-map))))))

(defn- make-query-cache [query-fn arg->cache-key]
  (if enable-query-cache
    (cache/make
     query-fn
     :key-fn (fn ->key
               ([arg-map]
                (update arg-map :args #(mapv arg->cache-key %)))
               ([q & args]
                (->key {:query q :args args})))
     :maximum-size 10000)
    query-fn))

(defn- datahike-arg->cache-key [x]
  (condp instance? x
    DB {:max-tx (:max-tx x)
        :id (:datahike/id (.-meta ^DB x))}
    AsOfDB {:as-of (.-time_point ^AsOfDB x)
            :db (datahike-arg->cache-key (.-origin_db ^AsOfDB x))}
    SinceDB {:since (.-time_point ^SinceDB x)
             :db (datahike-arg->cache-key (.-origin_db ^SinceDB x))}
    HistoricalDB {:history (datahike-arg->cache-key (.-origin_db ^HistoricalDB x))}
    FilteredDB {:pred (.-pred ^FilteredDB x)
                :db (datahike-arg->cache-key (.-unfiltered_db ^FilteredDB x))}
    x))

(defn db->cache-key [db]
  (datomic-impl/->query-arg (datahike-arg->cache-key db)))

;; Wrapping functions in the Datahike/Datomic interface
(def ^:private q-
  (backend-specific-fn
   :datomic (make-query-cache datomic/q datomic-impl/->query-arg)
   :datahike (make-query-cache datahike/q datahike-arg->cache-key)))
(def q q-)
;; Use this db wrapper function to log queries
;(defn q
;  [& params]
;  (let [res (apply q- params)
;        _ (println "query: " params "\nresp: " res "\n")
;    res)

(def transact
  (backend-specific-fn
   :datomic datomic/transact
   :datahike datahike/transact))
(def pull
  (backend-specific-fn
   :datomic datomic/pull
   :datahike datahike/pull))
(def history
  (backend-specific-fn
   :datomic datomic/history
   :datahike datahike/history))
(def tx-range
  (backend-specific-fn
   :datomic datomic/tx-range
   :datahike datahike-sendandi/tx-range))
(def as-of
  (backend-specific-fn
   :datomic datomic/as-of
   :datahike datahike/as-of))
(def db
  (backend-specific-fn
   :datomic datomic/db
   :datahike datahike/db))
(def get-client
  (backend-specific-fn
   :datomic (fn [cfg] (retry 1000 #(datomic/client cfg)))
   :datahike (fn [cfg] (datahike-sendandi/datahike-client cfg))))
(def connect
  (backend-specific-fn
   :datomic (fn [cfg] (retry 1000 #(datomic/connect (get-client cfg) {:db-name (:datomic-name cfg)})))
   :datahike (fn [cfg] (datahike/connect cfg))))

(def ^:private datahike-in-mem-config
  {:wanderung/type :datahike
   :store {:backend :mem
           :id "testing"}
   :schema-flexibility :write
   :attribute-refs? true
   :index :datahike.index/persistent-set
   :keep-history true
   :name "in-mem"})

(defn merge-datomic-backend [env]
  (conj (:datomic-cfg env)
        {:wanderung/type :datomic
         :name (:datomic-name (:datomic-cfg env))}))

(defstate ^{:on-reload :noop} conn
  :start
  (let [config (case db-backend
                 :datomic
                 (merge-datomic-backend env)
                 :datahike
                 (conj (:datahike-cfg env)
                       {:wanderung/type :datahike}))
        in-mem (:in-mem env)
        _ (println "start:conn" in-mem db-backend config)]
    (if in-mem
      (do
        (time (wanderung/migrate config datahike-in-mem-config))
        (alter-var-root #'jobtech-taxonomy-api.config/db-backend (constantly :datahike))
        (connect datahike-in-mem-config))
      (connect config)))
  :stop
  (println "stop:conn"))

(defn get-conn []
  (if (nil? conn)
    (throw (Exception. "DB service not started."))
    conn))

(def get-database-time-point-by-version-query
  '[:find ?tx
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn get-transaction-id-from-version [db version]
  (ffirst (q get-database-time-point-by-version-query db version)))

(def get-latest-released-version-query
  '[:find (max ?version)
    :in $
    :where [_ :taxonomy-version/id ?version]])

(defn get-latest-released-version [db]
  (ffirst (q get-latest-released-version-query db)))

;; This cannot use the conn var, as it will destroy the
;; integration tests (where the conn is made before the
;; tests fill the databases with test data).
(defn get-db
  "Get database, possibly at a point in time of some release.

  Optional version arg can be:
  - `:next` (default) - the latest database, includes unpublished changes
  - `:latest` - the database at a latest release time point
  - an int - the database at a time point of that release"
  ([]
   (get-db :next))
  ([version]
   (let [db (db (get-conn))]
     (cond
       (= :next version)
       db

       (= :latest version)
       (->> (or (get-latest-released-version db)
                (throw (ex-info "There are no releases yet"
                                {:db db})))
            (get-transaction-id-from-version db)
            (as-of db))

       (int? version)
       (as-of db (or (get-transaction-id-from-version db version)
                     (throw (ex-info "There is no such db version"
                                     {:version version
                                      :db db}))))

       :else
       (throw (ex-info "Invalid db version, use :latest, :next or int"
                       {:version version}))))))
