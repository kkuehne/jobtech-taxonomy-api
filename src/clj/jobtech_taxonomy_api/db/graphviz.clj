(ns jobtech-taxonomy-api.db.graphviz
  (:require [clojure.java.shell :as sh]
            [clojure.string :as str]
            [jobtech-taxonomy-api.cache :as cache]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.graphql.relation :as graphql.relation]
            [jobtech-taxonomy-common.relation :as relation]))

(def cached-sh (cache/make sh/sh :maximum-size 1000))

(defn- sh! [& args]
  (let [ret (apply cached-sh args)]
    (if (zero? (:exit ret))
      (:out ret)
      (throw (ex-info (format "Command '%s' failed:%n%s"
                              (->> args (take-while string?) (str/join " "))
                              (:err ret))
                      (assoc ret :args args))))))

(defn types-svg [{:keys [version]}]
  (let [edges (->> (db/q {:query '{:find [?t1 (distinct ?rt) ?t2]
                                   :keys [:from :relations :to]
                                   :where [[?r :relation/type ?rt]
                                           [?r :relation/concept-1 ?c1]
                                           (not [?c1 :concept/deprecated true])
                                           [?c1 :concept/type ?t1]
                                           [?r :relation/concept-2 ?c2]
                                           (not [?c2 :concept/deprecated true])
                                           [?c2 :concept/type ?t2]]}
                          :args [(db/get-db version)]})
                   (group-by (comp set (juxt :from :to)))
                   vals
                   (map
                    (fn [rows]
                      (let [sorted (sort-by #(-> % :relations count -) rows)
                            [{:keys [from relations to]} inv] sorted]
                        {:from from
                         :relations (into relations
                                          (map relation/relations)
                                          (:relations inv))
                         :to to})))
                   (map
                    (fn [{:keys [from relations to]}]
                      (str "  " (pr-str (str from)) " -> " (pr-str (str to))
                           " [decorate=true, label="
                           (->> relations
                                (map graphql.relation/type->field-label)
                                sort
                                (str/join "\n")
                                pr-str)
                           (when (every? #(= % (relation/relations %)) relations)
                             (str ", dir=none, constraint=false, color=\"#96b0ff\", fontcolor=\"#96b0ff\""))
                           "]")))
                   (str/join "\n"))]
    (sh! "dot" "-Tsvg" :in
         (format "
digraph TaxonomyTypes {
  label=\"Taxonomy Types and Relations\"
  rankdir=RL
  graph [fontname=\"Arial\",
         labelloc=t,
         fontsize=30,
         fontcolor=\"#00005a\",
         ranksep=0.5]
  node [shape=box,
        style=\"rounded,filled\",
        color=\"#92eb42\",
        fontcolor=\"#00005a\",
        fontname=\"Arial\",
        margin=\"0.1,0.05\",
        height=0.02,
        width=0.02]
  edge [color=\"#00005a\",
        fontcolor=\"#00005a\",
        fontname=\"Arial\"]
%s
}" edges))))