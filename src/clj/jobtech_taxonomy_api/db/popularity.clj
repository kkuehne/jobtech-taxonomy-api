(ns jobtech-taxonomy-api.db.popularity
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]))

(def ^:private popularity-delay
  (delay
   (with-open [r (io/reader (io/resource "popularity/freq_data.json"))]
     (json/read r))))

(defn fetch []
  @popularity-delay)
