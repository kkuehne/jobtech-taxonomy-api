(ns jobtech-taxonomy-api.db.search
  (:require [jobtech-taxonomy-api.db.popularity :as pop]
            [taoensso.nippy :as nippy]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-common.relation :as relation]
            [clojure.string :as str]
            [jobtech-taxonomy-api.cache :as cache]
            [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.db.concepts :as concepts])
  (:import
   [org.apache.lucene.search.suggest.analyzing FuzzySuggester]
   [org.apache.lucene.analysis Analyzer]
   [org.apache.lucene.analysis.standard StandardTokenizerFactory]
   [org.apache.lucene.analysis.miscellaneous ASCIIFoldingFilter]
   [org.apache.lucene.analysis.miscellaneous ASCIIFoldingFilterFactory]
   [org.apache.lucene.analysis.custom CustomAnalyzer]
   [org.apache.lucene.analysis.custom CustomAnalyzer]
   [org.apache.lucene.store MMapDirectory Directory]
   [java.nio.file Files LinkOption]
   [java.nio.file.attribute FileAttribute]
   [org.apache.lucene.search.suggest.analyzing AnalyzingInfixSuggester]
   [org.apache.lucene.search.suggest InputIterator Lookup Lookup$LookupResult]
   [org.apache.lucene.util BytesRef]
   [java.util Arrays]
   [org.apache.lucene.analysis.standard StandardAnalyzer]
   [clojure.lang RT]
   [org.apache.lucene.search.suggest.analyzing FuzzySuggester]
   [java.util.stream Collectors]))

(defn build-analyzer []
  (.build (doto (CustomAnalyzer/builder)
            (.withTokenizer "standard" (into-array String []))
            (.addTokenFilter "lowercase" (into-array String []))
            (.addTokenFilter "asciiFolding" (into-array String ["preserveOriginal" "true"])))))

(def ^Analyzer analyzer (build-analyzer))

(def ^Directory directory
  (let [dir (Files/createTempDirectory "taxonomy-lucene-index" (into-array FileAttribute []))]
    (.addShutdownHook
     (Runtime/getRuntime)
     (Thread. ^Runnable (fn []
                          (->> dir
                               (tree-seq
                                #(Files/isDirectory % (into-array LinkOption []))
                                #(.collect (Files/list %) (Collectors/toList)))
                               reverse
                               (run! #(Files/delete %))))))
    (MMapDirectory. dir)))

(defn- default-to-1 [_] 1)

(defn- ^InputIterator InputIterator-on
  [seqable & {:keys [text-fn weight-fn payload-fn contexts-fn]
              :or {text-fn str
                   weight-fn default-to-1
                   payload-fn nil
                   contexts-fn nil}}]
  (let [iter (RT/iter seqable)
        current-vol (volatile! nil)]
    (reify InputIterator
      (next [_]
        (when (.hasNext iter)
          (let [current (.next iter)]
            (vreset! current-vol current)
            (BytesRef. (str (text-fn current))))))
      (weight [_]
        (weight-fn @current-vol))
      (hasPayloads [_]
        (some? payload-fn))
      (payload [_]
        (BytesRef. ^bytes (nippy/freeze (payload-fn @current-vol))))
      (hasContexts [_]
        (some? contexts-fn))
      (contexts [_]
        (->> @current-vol contexts-fn (map #(BytesRef. (str %))) set)))))


;; Fuzzysuggester will break the API since it cant use context. But I save the code for refactoring
(defn create-fuzzy-suggester []
  (FuzzySuggester.
   directory
   "fuzzy"
   #_index-analyzer analyzer
   #_query-analyzer analyzer
   (bit-or FuzzySuggester/EXACT_FIRST  FuzzySuggester/PRESERVE_SEP)
   256,
   -1,
   true,
   FuzzySuggester/DEFAULT_MAX_EDITS,
   FuzzySuggester/DEFAULT_TRANSPOSITIONS,
   0,
   FuzzySuggester/DEFAULT_MIN_FUZZY_LENGTH,
   FuzzySuggester/DEFAULT_UNICODE_AWARE
   )
  )

(defn create-infix-suggester []
  (AnalyzingInfixSuggester.
   directory
   #_index-analyzer analyzer
   #_query-analyzer analyzer
   #_min-prefix-chars 4
   #_commit-on-build true
   #_all-terms-required false
   #_highlight true)
  )

(def ^:private lookup
  (cache/make
   (fn [db]
     (doto
         (create-infix-suggester)
       (.build (InputIterator-on
                (map first (db/q
                            '[:find (pull ?c [:concept/id
                                              :concept/type
                                              :concept/preferred-label
                                              :concept/alternative-labels])
                              :where [?c :concept/id ?id]]
                            db))
                :payload-fn identity
                :contexts-fn (juxt :concept/type :concept/id)
                :text-fn #(str (:concept/preferred-label %)
                               " "
                               (str/join " " (:concept/alternative-labels %)))))))
   :key-fn db/db->cache-key))

(defn get-concepts-by-search [{:keys [;;required
                                      query-string
                                      version
                                      ;; optional
                                      type ;; coll of strings
                                      relation
                                      related-ids ;; coll of strings
                                      offset
                                      limit]}]
  (let [db (db/get-db version)
        ^Lookup lookup (lookup db)
        results (->> (.lookup lookup
                              query-string
                              (->> (if (and relation (seq related-ids))
                                     (-> {:find '[?id]
                                          :args [db relation/rules]
                                          :where '[[?c :concept/id ?id]]
                                          :in '[$ %]
                                          :offset 0
                                          :limit -1}
                                         (concepts/handle-relations relation related-ids)
                                         concepts/remap-query
                                         db/q
                                         (->> (map first)))
                                     type)
                                   (map #(BytesRef. (str %)))
                                   set)
                              true
                              (or limit 100))
                     (map #(let [bytes-ref (.-payload ^Lookup$LookupResult %)]
                             (nippy/thaw (Arrays/copyOfRange (.-bytes bytes-ref)
                                                             (.-offset bytes-ref)
                                                             (+ (.-offset bytes-ref)
                                                                (.-length bytes-ref))))))
                     (filter (if (and (seq related-ids) relation (seq type))
                               (comp (set type) :concept/type)
                               any?)))

        pop (pop/fetch)

        ;; costs are from 0 to 1, lower is better

        max-pop (transduce
                 (map #(get pop (:concept/id %) 0))
                 max
                 1
                 results)
        popularity-cost #(- 1
                            (-> %
                                :concept/id
                                (as-> $ (get pop $ 0))
                                (/ max-pop)))

        starts-with-pattern (api-util/str-to-pattern-starts-with query-string)
        starts-with-cost #(if (.matches ^String (:concept/preferred-label %)
                                        starts-with-pattern)
                            0
                            1)]
    (->> results
         (sort-by (juxt starts-with-cost popularity-cost))
         (api-util/pagination offset limit))))

(comment

  (get-concepts-by-search
   {:query-string "köp"
    :version :latest})

  (get-concepts-by-search
   {:query-string "f"
    :version :latest
    :type ["ssyk-level-3" #_"occupation-field"]
    :relation "broader"
    :related-ids ["Fghp_zje_WA8"]}))
