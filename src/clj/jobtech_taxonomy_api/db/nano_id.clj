(ns jobtech-taxonomy-api.db.nano-id
  (:gen-class)
  (:require
   [nano-id.core :refer [custom]]))

;; Specify alphabet and length of nano ID
(def ^:private generate (custom "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ" 10))

(defn- add-underscore [id]
  (str (subs id 0 4) "_" (subs id 4 7)  "_" (subs id 7 10)))

(defn generate-new-id-with-underscore []
  (add-underscore (generate)))
