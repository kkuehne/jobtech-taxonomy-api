(ns jobtech-taxonomy-api.db.graphql.concept-type
  (:require [jobtech-taxonomy-api.db.graphql.impl :as graphql.impl]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [io.github.vlaaad.plusinia :as p]))

(def schema
  {:queries
   {:concept_types {:type '(non-null (list (non-null :ConceptType)))
                    :description "Get concept type metadata"
                    :args {:version {:type '(non-null :VersionRef)
                                     :description "Use this taxonomy version"
                                     :default-value :latest}}}}
   :objects {:ConceptType {:description "Concept type"
                           :fields {:id {:type '(non-null String)
                                         :description "Programmatic concept type identifier"}
                                    :label_sv {:type '(non-null String)
                                               :description "Swedish label for the concept type"}
                                    :label_en {:type '(non-null String)
                                               :description "English label for the concept type"}}}}})

(defn- fetch-types [{:keys [api-key version]} nils]
  (zipmap
    nils
    (repeat
      (db/q '[:find ?ct ?sv ?en
              :keys :id :label_sv :label_en
              :where (or-join [?ct ?sv ?en]
                              (and [?e :concept-type/id ?ct]
                                   [(get-else $ ?e :concept-type/label-sv ?ct) ?sv]
                                   [(get-else $ ?e :concept-type/label-en ?ct) ?en])
                              (and [_ :concept/type ?ct]
                                   (not [_ :concept-type/id ?ct])
                                   [(identity ?ct) ?sv]
                                   [(identity ?ct) ?en]))]
            (graphql.impl/get-db api-key version)))))

(def fetchers
  {:Query {:concept_types (p/make-query-fetcher fetch-types :context-keys #{:api-key})}
   :ConceptType {:id (graphql.impl/transform-fetcher :id)
                 :label_sv (graphql.impl/transform-fetcher :label_sv)
                 :label_en (graphql.impl/transform-fetcher :label_en)}})

