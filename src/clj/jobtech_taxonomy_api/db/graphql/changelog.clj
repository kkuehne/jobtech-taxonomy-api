(ns jobtech-taxonomy-api.db.graphql.changelog
  (:require [clojure.string :as str]
            [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.graphql.concept :as graphql.concept]
            [jobtech-taxonomy-api.db.graphql.impl :as graphql.impl]
            [jobtech-taxonomy-api.db.graphql.relation :as graphql.relation]
            [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-api.middleware :as middleware]
            [io.github.vlaaad.plusinia :as p]))

;; High-level overview of information flow when loading changes:
;; - `query` loads db events with tx-kinds - information that is enough to
;;   figure out types of events (Created, Updated, Deprecated, Commented), but
;;   not with information about what exactly has changed;
;; - `fetch-updated-event-changes` uses tx-kinds of Updated events to load
;;   information about the change - what concept attr or relation field were
;;   changed;
;; - `resolve-change-attribute` converts change information (concept attr or
;;   relation field) to GraphQL attribute name, `fetch-updated-event-changes`
;;   loads old/new value of the change (either concept attr or relation field).

(def ^:private ->inst-rules
  "Rule that transform various inputs to insts:
  - :next is resolved to inst of the latest db tx
  - :latest is resolved to inst of tx of the latest release
  - numbers are resolved to insts of txs of releases identified by the number
  - dates are resolved as themselves"
  '[[(->inst ?in ?ret)
     (or-join [?in ?ret]
              (and [(ground :next) ?in]
                   [(q '[:find (max ?inst)
                         :where [_ :db/txInstant ?inst]] $)
                    [[?ret]]])
              (and [(ground :latest) ?in]
                   [(q '[:find (max ?version)
                         :where [_ :taxonomy-version/id ?version]] $)
                    [[?version]]]
                   [?v :taxonomy-version/id ?version]
                   [?v :taxonomy-version/tx ?tx]
                   [?tx :db/txInstant ?ret])
              (and [(int? ?in)]
                   [?v :taxonomy-version/id ?in]
                   [?v :taxonomy-version/tx ?tx]
                   [?tx :db/txInstant ?ret])
              (and [(inst? ?in)]
                   [(identity ?in) ?ret]))]])

(defn query [db {:keys [from to id type limit offset]}]
  ;; For every permutation of concept and transaction there might be at most 4
  ;; events:
  ;; - Created - when concept is created;
  ;; - Updated - when some GraphQL concept field (except deprecated) is changed;
  ;; - Deprecated - when concept is deprecated;
  ;; - Commented - when some clarification about a concept was asserted;

  ;; `rows` are permutations of concept and transaction with minimum amount of
  ;; information needed to figure out these events:
  ;; - ?tx is a transaction entity;
  ;; - ?user, ?timestamp and ?comment are a part of ?tx entity;
  ;; - ?c is a concept entity;
  ;; - ?id is a part of concept entity;
  ;; - everything else (?tx-kind and ?relation) is aggregated per tx/concept
  ;;   permutation.
  ;; ?tx-kind describes a type of change asserted on a concept in a transaction,
  ;; it can be either:
  ;; - :create-concept - if `:concept/id` was asserted;
  ;; - :change-concept - if any attr of of a concept was asserted;
  ;; - :deprecate-concept - if `:concept/deprecated true` was asserted;
  ;; - :replace-concept - if `:concept/replaced-by` or `:concept/_replaced-by`
  ;;   were asserted;
  ;; - :change-relation - if there were changes to any attrs of relations of
  ;;   a concept;
  ;; - :concept-daynote - if there were daynotes created on a concept;
  ;; - :relation-daynote - if there were daynotes created on a relation of
  ;;   a concept.
  ;; ?relation is either:
  ;; - `false` for branches that don't have relations;
  ;; - relation entity id;
  ;; - `:replaces` or `:replaced_by`.
  ;; Every permutation of tx + concept can have different tx kinds and relations
  (let [rows (-> {:query '{:find [?user ?timestamp ?comment ?tx ?c ?id
                                  (distinct ?tx-kind) (distinct ?relation)]
                           :keys [:user :timestamp :comment :tx :concept :id :tx-kinds :relations]
                           :in [$ %]
                           :where []}
                  :args [(db/history db) ->inst-rules]}
                 (cond->
                   (seq id)
                   (graphql.impl/merge-into
                     {:query {:in '[[?id ...]]}
                      :args [id]})

                   (seq type)
                   (graphql.impl/merge-into
                     {:query {:where '[[?c :concept/type ?type]]
                              :in '[[?type ...]]}
                      :args [type]})

                   from
                   (graphql.impl/merge-into
                     {:query {:where ['[(identity ?from-input) ?from]
                                      '(->inst ?from ?from-inst)]
                              :in '[?from-input]}
                      :args [from]})

                   to
                   (graphql.impl/merge-into
                     {:query {:where ['[(identity ?to-input) ?to]
                                      '(->inst ?to ?to-inst)]
                              :in '[?to-input]}
                      :args [to]}))

                 (graphql.impl/merge-into
                   {:query
                    {:where
                     '[[?c :concept/id ?id]
                       (or-join
                         [?c ?tx ?tx-kind ?relation]
                         (and [?c _ _ ?tx]
                              (or-join
                                [?c ?tx ?tx-kind]
                                (and [?c :concept/id _ ?tx true]
                                     [(ground :create-concept) ?tx-kind])
                                (and [?c :concept/deprecated true ?tx true]
                                     [(ground :deprecate-concept) ?tx-kind])
                                (and (not [?c :concept/deprecated true ?tx true])
                                     [(ground :change-concept) ?tx-kind]))
                              [(ground false) ?relation])
                         (and [?c :concept/replaced-by _ ?tx]
                              [(ground :replace-concept) ?tx-kind]
                              [(ground :replaced_by) ?relation])
                         (and [_ :concept/replaced-by ?c ?tx]
                              [(ground :replace-concept) ?tx-kind]
                              [(ground :replaces) ?relation])
                         (and (or [?r :relation/concept-1 ?c]
                                  [?r :relation/concept-2 ?c])
                              [?r _ _ ?tx]
                              [(ground :change-relation) ?tx-kind]
                              [(identity ?r) ?relation])
                         (and [?tx :daynote/ref ?c]
                              [(ground :concept-daynote) ?tx-kind]
                              [(ground false) ?relation])
                         (and [?tx :daynote/ref ?r]
                              (or [?r :relation/concept-1 ?c]
                                  [?r :relation/concept-2 ?c])
                              [(ground :relation-daynote) ?tx-kind]
                              [(identity ?r) ?relation]))
                       [?tx :db/txInstant ?timestamp]
                       [(get-else $ ?tx :taxonomy-user/id "@system") ?user]
                       [(get-else $ ?tx :daynote/comment "") ?comment]]}})
                 (cond->
                   from
                   (graphql.impl/merge-into
                     {:query {:where [(if (inst? from)
                                        '[(<= ?from-inst ?timestamp)]
                                        '[(< ?from-inst ?timestamp)])]}})
                   to
                   (graphql.impl/merge-into
                     {:query {:where [(if (inst? to)
                                        '[(< ?timestamp ?to-inst)]
                                        '[(<= ?timestamp ?to-inst)])]}}))
                 db/q)
        txs (->> rows
                 (map :tx)
                 distinct
                 sort)]
    (if (seq txs)
      ;; Since all changes allow loading old value and new value, we need to
      ;; add db-before/db-after for every row to query these values from.
      ;; For db-after, it's enough to create a map from tx to corresponding
      ;; as-of db.
      ;; For db-before, we can use dbs as-of previous tx. The only problem is
      ;; that we also need to know previous tx - `prev-tx` - of a very first
      ;; tx in rows.
      (let [prev-tx (ffirst (db/q '[:find (max ?prevTx)
                                    :in $ ?tx
                                    :where
                                    [?tx :db/txInstant ?inst]
                                    [?prevTx :db/txInstant ?prevInst]
                                    [(< ?prevInst ?inst)]]
                                  db (first txs)))
            tx->prev-tx (->> txs
                             (cons prev-tx)
                             (partition 2 1)
                             (map (fn [[prev-tx tx]]
                                    [tx prev-tx]))
                             (into {}))
            tx->db (->> txs
                        (cons prev-tx)
                        (map (juxt identity #(db/as-of db %)))
                        (into {}))]
        (->> rows
             (map #(update % :relations disj false))        ;; useless
             (sort-by (juxt :timestamp :id))
             (mapcat
               (fn [{:keys [tx-kinds tx] :as event}]
                 (let [event (assoc event :db-before (tx->db (tx->prev-tx tx))
                                          :db-after (tx->db tx))]
                   (cond-> []
                           (:create-concept tx-kinds)
                           (conj (p/make-node event :type :Created :context {:db db}))

                           (and (not (:create-concept tx-kinds))
                                (or (:change-concept tx-kinds)
                                    (:change-relation tx-kinds)
                                    (:replace-concept tx-kinds)))
                           (conj (p/make-node event :type :Updated :context {:db db}))

                           (:deprecate-concept tx-kinds)
                           (conj (p/make-node event :type :Deprecated :context {:db db}))

                           (or (:relation-daynote tx-kinds)
                               (:concept-daynote tx-kinds))
                           ;; TODO: for relation daynotes, modify the comment to
                           ;;       mention a relation
                           (conj (p/make-node event :type :Commented :context {:db db}))))))
             (api-util/pagination offset limit)))
      [])))

(defn- wrap-any [x]
  (if (some? x)
    {:jobtech-taxonomy-api.db.graphql/any-wrapper x}
    x))

(defn- wrap-any-vals [m]
  (update-vals m wrap-any))

(def ^:private event-fields
  {:user {:type '(non-null String)}
   :timestamp {:type '(non-null :Date)}
   :comment {:type '(non-null String)}
   :concept {:type '(non-null :Concept)}})

(def schema
  {:queries
   {:changelog
    {:type '(non-null (list (non-null :ChangelogEvent)))
     :description "Fetch concept changes over a period of time"
     :args {:from {:type :DateRef
                   :description "Load changes starting from this date ref, inclusive for dates and exclusive for versions"
                   :default-value 0}
            :to {:type :DateRef
                 :description "Load changes up to this date ref, exclusive for dates and inclusive for versions"
                 :default-value :latest}
            :id {:type '(list (non-null String))
                 :description "Restrict results to changes to these concept IDs"}
            :type {:type '(list (non-null String))
                   :description "Restrict results to changes to these concept types"}
            :limit {:type 'Int
                    :description "Pagination: maximum amount of returned changes"}
            :offset {:type 'Int
                     :description "Pagination: skip this many returned changes"}}}}

   :scalars
   {:DateRef {:description "Date identifier, either Date in `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'` format or version identifier (i.e. \"latest\", \"next\" or version number)"
              :parse #(if (and (string? %) (str/includes? % "-"))
                        (graphql.impl/parse-date %)
                        (graphql.impl/parse-version-ref %))
              :serialize #(if (inst? %)
                            (graphql.impl/serialize-date %)
                            (graphql.impl/serialize-version-ref %))}
    :Any {:description "Anything"
          ;; lacinia wants `fn?` instead of `ifn?`
          :serialize #(get % :jobtech-taxonomy-api.db.graphql/any-wrapper)
          :parse identity}}

   :interfaces
   {:ChangelogEvent
    {:description "Some change to a concept"
     :fields event-fields}}

   :objects
   {:Created
    {:implements [:ChangelogEvent]
     :description "Concept is created"
     :fields event-fields}

    :Updated
    {:implements [:ChangelogEvent]
     :description "Concept is updated"
     :fields (assoc event-fields
               :changes {:type '(non-null (list (non-null :Change)))
                         :description "Changes list"})}

    :Deprecated
    {:implements [:ChangelogEvent]
     :description "Concept is deprecated"
     :fields event-fields}

    :Commented
    {:implements [:ChangelogEvent]
     :description "Some property of a concept is clarified without any changes"
     :fields event-fields}

    :Change
    {:description "Concept change description"
     :fields
     {:attribute {:type '(non-null String)
                  :description "`Concept` field name"}
      :old_value {:type :Any
                  :description "Field value before the change. Relations always include deprecated concepts."}
      :new_value {:type :Any
                  :description "Field value after the change. Relations always include deprecated concepts."}}}}})

(defn- fetch-changelog [{:keys [api-key from to] :as args} nils]
  (zipmap
    nils
    (repeat
      (if (and (or (= :next from) (= :next to))
               (not (middleware/authenticate-admin api-key)))
        (throw (ex-info "Not authorized" {:api-key api-key}))
        (query (db/get-db) args)))))

(defn fetch-updated-event-changes [{:keys [db]} values]
  ;; Now we need to transform event rows of :Updated events to change lists.
  ;; As per logic of `query`, :Updated event may contain several of the
  ;; following tx kinds:
  ;; - :change-concept
  ;; - :change-relation
  ;; - :replace-concept
  (let [hist-db (db/history db)
        ;; for rows with :change-concept tx kind, load changed attrs
        concept+tx->attrs (->> (db/q
                                 '[:find ?c ?tx (distinct ?attr)
                                   :keys :concept :tx :attrs
                                   :in $ [[?c ?tx]] [?attr ...]
                                   :where
                                   [?c ?a _ ?tx]
                                   [?a :db/ident ?attr]]
                                 hist-db
                                 (->> values
                                      (filter #(-> % :tx-kinds :change-concept))
                                      (map (juxt :concept :tx)))
                                 (keys graphql.concept/fetched-attrs))
                               (map (juxt (juxt :concept :tx) :attrs))
                               (into {}))
        ;; for rows with :change-relation tx kind, load relation types
        concept+tx->relation-types (->> (db/q
                                          '[:find ?c ?tx (distinct ?type)
                                            :keys :concept :tx :relation-types
                                            :in $ % [[?c ?tx ?r]]
                                            :where
                                            (edge ?c ?type _ ?r)
                                            [?r _ _ ?tx]]
                                          hist-db
                                          relation/rules
                                          (for [{:keys [tx-kinds tx concept relations]} values
                                                :when (:change-relation tx-kinds)
                                                rel relations
                                                :when (int? rel)]
                                            [concept tx rel]))
                                        (map (juxt (juxt :concept :tx) :relation-types))
                                        (into {}))]
    (->> values
         (map
           (juxt
             identity
             (fn [{:keys [concept tx tx-kinds relations] :as event}]
               ;; produce change objects, that are event objects augmented with
               ;; some more data:
               ;; - :change - a type of change that attribute/old_value/new_value
               ;;   resolvers need to be aware of, either:
               ;;   - `:attr` for changes to GraphQL fields loaded using
               ;;     [[graphql.concept]] ns;
               ;;   - `:relation` for changes to GraphQL fields loaded using
               ;;     [[graphql.relation]] ns;
               ;; - `:attr` - db attribute for `:change :attr` changes;
               ;; - `:field-id` - GraphQL field id for `:change :relation`
               ;;    changes.
               (concat
                 (map #(assoc event :change :attr
                                    :attr %)
                      (concept+tx->attrs [concept tx]))
                 (when (:replace-concept tx-kinds)
                   (for [rel relations
                         :when (keyword? rel)]
                     (assoc event :change :relation
                                  :field-id rel)))
                 (map #(assoc event :change :relation
                                    :field-id (graphql.relation/type->field-id %))
                      (concept+tx->relation-types [concept tx]))))))
         (into {}))))

(defn- fetch-change-attribute [{:keys [change attr field-id]}]
  (case change
    :attr (graphql.concept/attr->field-label attr)
    :relation (name field-id)))

(defn- change-fetcher [db-key]
  (p/make-field-fetcher
    (fn [{:keys [change db attr field-id]} events]
      (wrap-any-vals
        (case change
          :attr ((graphql.concept/field-fetcher attr) {:db db} events)
          :relation ((graphql.relation/field-id->fetcher field-id) {:db db :include_deprecated true} events))))
    :batch-fn (fn [ctx args event]
                (merge ctx args {:db (get event db-key)
                                 :change (:change event)
                                 :attr (:attr event)
                                 :field-id (:field-id event)}))))

(def fetchers
  {:Query {:changelog (p/make-query-fetcher fetch-changelog :context-keys #{:api-key})}
   :ChangelogEvent {:user (graphql.impl/transform-fetcher :user)
                    :timestamp (graphql.impl/transform-fetcher :timestamp)
                    :comment (graphql.impl/transform-fetcher :comment)
                    :concept (graphql.impl/transform-fetcher #(p/make-node (select-keys % [:id]) :context {:db (:db-after %)}))}
   :Updated {:changes fetch-updated-event-changes}
   :Change {:attribute (graphql.impl/transform-fetcher fetch-change-attribute)
            :old_value (change-fetcher :db-before)
            :new_value (change-fetcher :db-after)}})