(ns jobtech-taxonomy-api.db.daynotes
  (:refer-clojure :exclude [type])
  (:require
   [jobtech-taxonomy-api.db.api-util :as api-util]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.database-connection :as db]))

(defn- conj-in [m & path+vals]
  (->> path+vals
       (partition 2)
       (reduce #(update-in %1 (first %2) conj (second %2)) m)))

(def ^:private  show-version-instance-ids-query
  '[:find ?tx ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn- get-tx-version-map [db]
  (into (sorted-map) (db/q show-version-instance-ids-query db)))

(defn- convert-transaction-id-to-version-id
  "Return the version that TRANSACTION-ID belongs to.
    If an unpublished transaction, return -1."
  [tx-id->version transaction-id]
  (or (second (first (subseq tx-id->version >= transaction-id)))
      -1))

(def ^:private fetch-concepts-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label
                    :concept/alternative-labels
                    :concept/hidden-labels])
    :in $ [?id ...]
    :where [?e :concept/id ?id]])

(defn- build-fetch-relation-txs-query [hist-db concept-id from-timestamp to-timestamp]
  (-> {:query '{:find [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage]
                :keys [tx timestamp user-id comment relation substitutability_percentage]
                :in [$]
                :where [[?c :concept/id ?input-concept-id]
                        (or [?r :relation/concept-1 ?c]
                            [?r :relation/concept-2 ?c])
                        (or [?r _ _ ?tx]
                            [?tx :daynote/ref ?r])
                        [(get-else $ ?r :relation/substitutability-percentage 0) ?substitutability_percentage]
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                        [(get-else $ ?tx :daynote/comment false) ?comment]
                        [?tx :db/txInstant ?inst]]}
       :args [hist-db]}
      (cond->
       concept-id
        (conj-in [:query :in] '?input-concept-id
                 [:args] concept-id)

        from-timestamp
        (conj-in [:query :in] '?from-inst
                 [:query :where] '[(<= ?from-inst ?inst)]
                 [:args] from-timestamp)

        to-timestamp
        (conj-in [:query :in] '?to-inst
                 [:query :where] '[(< ?inst ?to-inst)]
                 [:args] to-timestamp))))

(defn- fetch-relation-txs [hist-db concept-id from-timestamp to-timestamp]
  (-> (build-fetch-relation-txs-query hist-db concept-id from-timestamp to-timestamp)
      db/q))

(defn- build-relation-changes-query
  [hist-db db tx+relations]
  {:query '[:find ?tx ?r ?t ?added
            (pull $latest ?c1 [:concept/id :concept/type :concept/preferred-label])
            (pull $latest ?c2 [:concept/id :concept/type :concept/preferred-label])
            :keys tx relation relation-type added source target
            :in $history $latest [[?tx ?r] ...]
            :where
            [$history ?r :relation/type ?t ?tx ?added]
            [$history ?r :relation/concept-1 ?c1]
            [$history ?r :relation/concept-2 ?c2]]
   :args [hist-db db tx+relations]})

(defn- fetch-relation-changes [hist-db db tx+relations]
  (group-by (juxt :tx :relation)
            (db/q (build-relation-changes-query hist-db db tx+relations))))

(defn get-for-relation [concept-id from-timestamp to-timestamp offset limit show-unpublished]
  (let [db (db/get-db)
        tx-id->version (get-tx-version-map db)
        hist-db (db/history db)
        relation+txs (fetch-relation-txs hist-db concept-id from-timestamp to-timestamp)
        tx+relation->changes (fetch-relation-changes hist-db db (map (juxt :tx :relation) relation+txs))]
    (->> relation+txs
         (sort-by :tx)
         (filter #(or show-unpublished (not= (convert-transaction-id-to-version-id tx-id->version (:tx %)) -1)))
         (api-util/pagination offset limit)
         (reduce (fn [acc {:keys [tx timestamp user-id comment relation substitutability_percentage]}]
                   (let [change (first (tx+relation->changes [tx relation]))
                         state (or (not-empty (select-keys change [:relation-type :source :target]))
                                   (get-in acc [:relation->latest-state relation])
                                   (db/pull (db/as-of db tx)
                                            '[(:relation/type :as :relation-type)
                                              {(:relation/concept-1 :as :source) [:concept/id :concept/type :concept/preferred-label]
                                               (:relation/concept-2 :as :target) [:concept/id :concept/type :concept/preferred-label]}]
                                            relation))
                         event (cond-> {:event-type (cond
                                                      (nil? change) "COMMENTED"
                                                      (:added change) "CREATED"
                                                      :else "DEPRECATED")
                                        :timestamp timestamp
                                        :user-id user-id
                                        :relation state
                                        :version (convert-transaction-id-to-version-id
                                                  tx-id->version tx)}
                                 comment (assoc :comment comment)
                                 substitutability_percentage
                                 (assoc :substitutability-percentage substitutability_percentage)
                                 show-unpublished (assoc :transaction-id tx))]
                     (-> acc
                         (update :events conj event)
                         (assoc-in [:relation->latest-state relation] state))))
                 {:relation->latest-state {}
                  :events []})
         :events)))

(defn create-for-relation [user-id comment concept-1 concept-2 relation-type]
  (when-let [e (concepts/fetch-relation-entity-id-from-concept-ids-and-relation-type concept-1 concept-2 relation-type)]
    (db/transact (db/get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                          {:db/id "datomic.tx"
                                           :daynote/ref e}]})))

(defn- fetch-concept-txs [hist-db concept-id from-timestamp to-timestamp]
  (-> {:query '{:find [?tx ?inst ?user ?comment ?c ?id]
                :keys [tx inst user comment concept concept-id]
                :in [$]
                :where [[?c :concept/id ?id]
                        (or [?c _ _ ?tx]
                            [?tx :daynote/ref ?c])
                        [?tx :db/txInstant ?inst]
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user]
                        [(get-else $ ?tx :daynote/comment false) ?comment]]}
       :args [hist-db]}
      (cond->
       concept-id
        (conj-in [:query :in] '?id
                 [:args] concept-id)

        from-timestamp
        (conj-in [:query :in] '?from-inst
                 [:query :where] '[(<= ?from-inst ?inst)]
                 [:args] from-timestamp)

        to-timestamp
        (conj-in [:query :in] '?to-inst
                 [:query :where] '[(< ?inst ?to-inst)]
                 [:args] to-timestamp))
      db/q))

(defn- fetch-concept-changes [hist-db tx+concepts]
  (group-by (juxt :tx :concept)
            (db/q '[:find ?tx ?c ?attr ?v ?added
                    :keys tx concept a v added
                    :in $ [[?tx ?c] ...]
                    :where
                    [?c ?a ?v ?tx ?added]
                    [?a :db/ident ?attr]]
                  hist-db tx+concepts)))

(defn- changes->deprecated-event [changes]
  (when (some #(and (:added %) (#{:concept/deprecated :concept/replaced-by} (:a %)) (:v %)) changes)
    {:event-type "DEPRECATED"}))

(defn- changes->commented-event [changes]
  (when (empty? changes)
    {:event-type "COMMENTED"}))

(defn- changes-to-new-concept [changes]
  (reduce (fn [acc {attr :a val :v}]
            (case attr
              :concept/alternative-labels (update acc attr conj val)
              :concept/hidden-labels (update acc attr conj val)
              (assoc acc attr val)))
          {}
          changes))

(defn- changes->created-event [changes]
  (when (some #(and (:added %) (= :concept/id (:a %))) changes)
    {:event-type "CREATED"
     :new-concept (changes-to-new-concept changes)}))

(defn- get-concept-id [db eid]
  (ffirst (db/q {:query '[:find ?cid
                          :in $ ?eid
                          :where
                          [?eid :concept/id ?cid]]
                 :args [db eid]})))

(defn- changes->updated-event [changes db]
  {:event-type "UPDATED"
   :concept-attribute-changes (->> changes
                                   (group-by :a)
                                   (map (fn [[attr changes]]
                                          (let [added (->> changes (filter :added) (map :v))
                                                removed (->> changes (remove :added) (map :v))
                                                old-value (first removed)
                                                new-value (first added)]
                                            (if (not= attr :concept/replaced-by)
                                              {:attribute (name attr)
                                               :old-value old-value
                                               :new-value new-value
                                               :removed removed
                                               :added added}
                                              {:attribute (name attr)
                                               :old-value (some->> old-value (get-concept-id db))
                                               :new-value (some->> new-value (get-concept-id db))
                                               :removed (map #(get-concept-id db %) removed)
                                               :added (map #(get-concept-id db %) added)})))))})

(defn- changes->event [changes db]
  (or (changes->commented-event changes)
      (changes->deprecated-event changes)
      (changes->created-event changes)
      (changes->updated-event changes db)))

(defn- daynotes-txs->present-concepts-map [db daynote-txs]
  (let [ids (map :concept-id daynote-txs)
        concepts (db/q fetch-concepts-query db ids)]
    (->> concepts
         (map first)
         (map (juxt :concept/id identity))
         (into {}))))

(defn get-for-concept [concept-id from-timestamp to-timestamp offset limit show-unpublished]
  (let [db (db/get-db)
        hist-db (db/history db)
        tx-id->version (get-tx-version-map db)
        daynote-txs (fetch-concept-txs hist-db concept-id from-timestamp to-timestamp)
        present-concepts-map (daynotes-txs->present-concepts-map db daynote-txs)
        tx+concept->changes (fetch-concept-changes hist-db (mapv (juxt :tx :concept) daynote-txs))]
    (->> daynote-txs
         (sort-by :tx)
         (filter #(or show-unpublished (not= (convert-transaction-id-to-version-id tx-id->version (:tx %)) -1)))
         (api-util/pagination offset limit)
         (map (fn [{:keys [tx concept inst user comment concept-id]}]
                (-> [tx concept]
                    tx+concept->changes
                    (changes->event db)
                    (assoc
                     :timestamp inst
                     :user-id user
                     :latest-version-of-concept (get present-concepts-map  concept-id)
                     :version (convert-transaction-id-to-version-id
                               tx-id->version tx))
                    (cond->
                     comment (assoc :comment comment)
                     show-unpublished (assoc :transaction-id tx))))))))

(defn create-for-concept [concept-id user-id comment]
  (db/transact (db/get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                        {:db/id "datomic.tx"
                                         :daynote/ref [:concept/id concept-id]}]}))

(def ^:private version-timestamp-query
  '[:find ?inst
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version ?tx]
    [?tx :db/txInstant ?inst]])

(defn- convert-version-id-to-timestamp [version-id]
  (ffirst (db/q version-timestamp-query (db/get-db) version-id)))

(defn- get-changes "get-fun is either get-for-concept or get-for-relation"
  [get-fun after-version to-version-inclusive offset limit show-unpublished]
  (get-fun
   nil
   (convert-version-id-to-timestamp after-version)
   (when to-version-inclusive (convert-version-id-to-timestamp to-version-inclusive))
   offset limit show-unpublished))

(defn- get-changes-with-pagination [get-fun after-version to-version-inclusive offset limit show-unpublished]
  (get-changes get-fun after-version to-version-inclusive offset limit show-unpublished))

(defn- strip-public-concept-changes [event]
  (dissoc event :user-id :comment :timestamp))

(defn- get-changes-with-pagination-public [get-fun after-version to-version-inclusive offset limit show-unpublished]
  (let [events (get-changes-with-pagination
                get-fun
                after-version to-version-inclusive
                offset limit show-unpublished)]
    (map strip-public-concept-changes events)))

(defn get-concept-changes-with-pagination-private [after-version to-version-inclusive offset limit]
  (get-changes-with-pagination get-for-concept after-version to-version-inclusive offset limit true))

(defn get-concept-changes-with-pagination-public [after-version to-version-inclusive offset limit]
  (get-changes-with-pagination-public get-for-concept after-version to-version-inclusive offset limit false))

(defn get-relation-changes-with-pagination-private [after-version to-version-inclusive offset limit]
  (get-changes-with-pagination get-for-relation after-version to-version-inclusive offset limit true))

(defn get-relation-changes-with-pagination-public [after-version to-version-inclusive offset limit]
  (get-changes-with-pagination-public get-for-relation after-version to-version-inclusive offset limit false))
