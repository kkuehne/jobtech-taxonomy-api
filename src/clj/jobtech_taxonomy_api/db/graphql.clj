(ns jobtech-taxonomy-api.db.graphql
  (:require [io.github.vlaaad.plusinia :as p]
            [com.walmartlabs.lacinia.schema :as l.schema]
            [com.walmartlabs.lacinia :as l]
            [jobtech-taxonomy-api.db.graphql.impl :as graphql.impl]
            [jobtech-taxonomy-api.db.graphql.concept :as graphql.concept]
            [jobtech-taxonomy-api.db.graphql.relation :as graphql.relation]
            [jobtech-taxonomy-api.db.graphql.version :as graphql.version]
            [jobtech-taxonomy-api.db.graphql.concept-type :as graphql.concept-type]
            [jobtech-taxonomy-api.db.graphql.changelog :as graphql.changelog]))

(def schema
  (l.schema/compile
    (p/wrap-schema
      (graphql.impl/deep-merge
        graphql.impl/schema
        graphql.concept/schema
        graphql.relation/schema
        graphql.version/schema
        graphql.concept-type/schema
        graphql.changelog/schema)
      (graphql.impl/deep-merge
        graphql.concept/fetchers
        graphql.relation/fetchers
        graphql.version/fetchers
        graphql.concept-type/fetchers
        graphql.changelog/fetchers))))

(defn execute [query variables operation-name api-key]
  (l/execute schema query variables {:api-key api-key} {:operation-name operation-name}))