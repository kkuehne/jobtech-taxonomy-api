(ns jobtech-taxonomy-api.db.versions
  (:require
   [jobtech-taxonomy-api.db.database-connection :as db]))

(def ^:private show-all-versions
  '{:find [?version ?inst]
    :keys [version timestamp]
    :in [$]
    :where [[?t :taxonomy-version/id ?version]
            [?t :taxonomy-version/tx ?tx]
            [?tx :db/txInstant ?inst]]})

(def ^:private show-versions-without-0
  ;; 0 is a dummy version to make it easier to code queries
  (update show-all-versions :where conj '[(not= ?version 0)]))

(def ^:private show-version-by-id
  (update show-all-versions :in conj '?version))

(def show-latest-version-id
  '[:find (max ?version)
    :where [_ :taxonomy-version/id ?version]])

(defn get-all-versions []
  (sort-by :version (db/q show-versions-without-0 (db/get-db))))

(defn get-current-version-id [db]
  (ffirst (db/q show-latest-version-id db)))

(defn- is-new-version-id-correct? [current-version-id new-version-id]
  (= new-version-id (inc (or current-version-id -1))))

(defn- get-version-timestamp [db version-id]
  (ffirst (db/q '[:find ?inst
                  :in $ ?version
                  :where
                  [?e :taxonomy-version/id ?version]
                  [?e :taxonomy-version/tx ?tx]
                  [?tx :db/txInstant ?inst]]
                db
                version-id)))

(defn- find-tx-at-time [db conn inst]
  (let [tx-instant-a (ffirst (db/q '[:find ?e
                                     :where [?e :db/ident :db/txInstant]]
                                   db))]
    (->> (db/tx-range conn {:start inst})
         first
         :tx-data
         (some #(when (= tx-instant-a (:a %)) (:e %))))))

(defn- after-release? [db version inst]
  (neg? (compare (get-version-timestamp db version) inst)))

(defn create-new-version
  ([id]
   (create-new-version id nil))
  ([id inst]
   (let [conn (db/get-conn)
         db (db/db conn)
         old-id (get-current-version-id db)]
     (cond
       (not (is-new-version-id-correct? old-id id))
       ::incorrect-new-version

       (and inst old-id (not (after-release? db old-id inst)))
       ::new-version-before-release

       :else
       (let [tx (or (and inst (find-tx-at-time db conn inst))
                    "datomic.tx")
             tx-result (db/transact conn {:tx-data [{:taxonomy-version/id id
                                                     :taxonomy-version/tx tx}]})]
         (first (db/q show-version-by-id (:db-after tx-result) id)))))))
