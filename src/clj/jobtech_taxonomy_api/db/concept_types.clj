(ns jobtech-taxonomy-api.db.concept-types
  (:require [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.api-util :as api-util]))

(defn assert-type [{:keys [concept-type label-sv label-en user-id]}]
  (db/transact (db/get-conn)
               {:tx-data [(api-util/user-action-tx user-id nil)
                          (cond-> {:concept-type/id concept-type}
                            label-sv
                            (assoc :concept-type/label-sv label-sv)
                            label-en
                            (assoc :concept-type/label-en label-en))]})
  {:success true})