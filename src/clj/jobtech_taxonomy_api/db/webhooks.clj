(ns jobtech-taxonomy-api.db.webhooks
  (:require
   [jobtech-taxonomy-api.db.database-connection :as db]))

(defn update-webhooks [key url]
  (db/transact (db/get-conn) {:tx-data [{:webhook/key key, :webhook/url url}]}))

(defn list-webhooks! []
  (let [db (db/get-db)
        result (db/q {:query '[:find (pull ?e [:webhook/key :webhook/url])
                               :where [?e :webhook/key _]]
                      :args [db]})]
    (apply concat result)))

(defn webhook-exists? [key]
  (not (empty? (filter #(= (:webhook/key %) key) (list-webhooks!)))))

(defn retract-webhook
  "FIXME: not ready - but not used by API neither."
  [key]
  (when (webhook-exists? key)
    (let [conn (db/get-conn)]
      (db/transact conn {:tx-data [[:db/retract :webhook/key key]]}))))

;; (update-webhooks "1" "http://example1.com")
;; (webhook-exists? "1111")
;; (list-webhooks!)
;; (retract-webhook "1")
