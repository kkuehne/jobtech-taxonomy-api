(ns jobtech-taxonomy-api.db.eures
  (:require
   [clojure.spec.alpha :as sp]
   [clojure.string :as str]
   [clojure.walk :as w]
   [jobtech-taxonomy-api.db.graphql :as graphql]
   [spec-tools.core :as st]
   [spec-tools.data-spec :as ds]
   [taxonomy :as types]))

(defn esco-occupation-by-id-and-version-query [id version]
  (format  "query MyQuery {
  concepts(type: \"occupation-name\", id: \"%s\", version: \"%s\", include_deprecated: true ) {
    id
    preferred_label
    type
    exact_match{
      id
      esco_uri
      preferred_label
      type
    }
    broad_match {
      id
      esco_uri
      preferred_label
      type
    }
    close_match {
      id
      esco_uri
      preferred_label
      type
    }
    narrow_match{
      id
      esco_uri
      preferred_label
      type
    }
    broader(type: \"isco-level-4\"){
      id
      preferred_label
      type
      esco_uri
    }
  }
}" id version))

(defn get-esco-occupation-by-id-and-version [id version]
  (graphql/execute (esco-occupation-by-id-and-version-query id version)
                   nil
                   nil
                   ""))

(defn parse-esco-occupations-concepts [response]
  (get-in response [:data :concepts]))

(defn convert-concept-to-esco-ids  [concept-with-relations]
  (let [concept concept-with-relations
        broad-match   (:broad_match concept)
        close-match   (:close_match concept)
        exact-match   (:exact_match concept)
        narrow-match  (:narrow_match concept)

        broad-match-count  (count broad-match)
        close-match-count  (count close-match)
        exact-match-count  (count exact-match)
        narrow-match-count (count narrow-match)

        total-match-count (+ broad-match-count close-match-count exact-match-count narrow-match-count)
        taxonomy-concept (:id concept)

        eures-concepts (if (< 1 total-match-count)
                         (if (#{1 2 3} exact-match-count)
                           exact-match
                           (if (< 3 total-match-count)
                             (:broader concept)
                             (if (not-empty narrow-match)
                               (:broader concept)
                               (concat broad-match close-match exact-match narrow-match))))
                         (concat broad-match close-match exact-match narrow-match))]

    [taxonomy-concept (map :esco_uri eures-concepts)]))

(defn handle-version [version]
  (if (nil? version)
    "latest"
    (if (> 6 version)
      "6"
      version)))

(defn lookup-occupation-name-id-and-version-to-esco-id [concept-id version]
  (let [concept-with-relations
        (first (parse-esco-occupations-concepts
                (get-esco-occupation-by-id-and-version concept-id (handle-version version))))
        esco-ids (second (convert-concept-to-esco-ids concept-with-relations))]
    (if (empty? esco-ids)
      (map :esco_uri (:broader concept-with-relations))
      esco-ids)))

(defn  handler [{{{:keys [id version]} :query} :parameters}]
  {:status 200
   :body (lookup-occupation-name-id-and-version-to-esco-id id version)})

(def responses
  (merge (types/response200 (sp/coll-of string?)) types/response404))
