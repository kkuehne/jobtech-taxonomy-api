(ns jobtech-taxonomy-api.db.concepts
  (:refer-clojure :exclude [type])
  (:require [clojure.string :as s]
            [clojure.tools.logging :as log]
            [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.nano-id :as nano]
            [jobtech-taxonomy-api.routes.parameter-util :as pu]
            [jobtech-taxonomy-common.relation :as relation]
            [taxonomy :as types]))

;; To understand the idea behind the following code read this blog post
;; https://grishaev.me/en/datomic-query/


(defn- sort-reply
  "Sort a collection of maps according to key :concept/sort-order"
  [reply]
  (if (contains? (first reply) :concept/sort-order)
    (sort-by :concept/sort-order reply)
    reply))

(def ^:private concept-pull-pattern
  [:concept/id
   :concept/type
   :concept/definition
   :concept/preferred-label
   :concept/deprecated
   :concept/quality-level
   :concept/sort-order
   :concept/alternative-labels
   :concept/hidden-labels
   {:concept/replaced-by [:concept/id
                          :concept/definition
                          :concept/type
                          :concept/preferred-label
                          :concept/deprecated]}])

(def ^:private initial-concept-query
  '{:find [(pull ?c pull-pattern)]
    :keys [concept]
    :in [$ % pull-pattern]
    :args []
    :where []
    :offset 0
    :limit -1})

(defn remap-query
  [{args :args offset :offset limit :limit :as m}]
  {:query (-> m
              (dissoc :args)
              (dissoc :offset)
              (dissoc :limit))
   :args args
   :offset offset
   :limit limit})

(defn handle-relations [query relation related-ids]
  ;; support substitutability-{from/to} for compatibility
  (let [relation ({"substitutability-from" "substituted-by"
                   "substitutability-to" "substitutability"} relation relation)]
    (-> query
        (update :in conj '?relation '[?related-ids ...])
        (update :args conj relation related-ids)
        (update :where into '[[?cr :concept/id ?related-ids]
                              (edge ?cr ?relation ?c ?_)]))))

(defn handle-deprecated [query deprecated include-deprecated]
  (let [inc-dep (if (= include-deprecated nil) deprecated include-deprecated)]
    (if inc-dep
      query
      (-> query
          (update :where conj '(not [?c :concept/deprecated true]))))))

(defn make-find-concepts-query [{:keys [id preferred-label type deprecated include-deprecated
                                        relation related-ids
                                        offset limit
                                        db pull-pattern
                                        extra-where-attributes
                                        include-legacy-information]}]
  {:pre [pull-pattern]}
  (cond-> initial-concept-query
    true
    (update :args conj db relation/rules pull-pattern)

    (and relation related-ids)
    (handle-relations relation related-ids)

    id
    (-> (update :in conj '?id)
        (update :args conj id)
        (update :where conj '[?c :concept/id ?id]))

    preferred-label
    (-> (update :in conj '?case-insensitive-preferred-label)
        (update :args conj (api-util/str-to-pattern-ignore-case preferred-label))
        (update :where conj '[?c :concept/preferred-label ?preferred-label])
        (update :where conj '[(.matches ^String ?preferred-label ?case-insensitive-preferred-label)]))

    type
    (-> (update :in conj '[?type ...])
        (update :args conj (if (vector? type) type [type]))
        (update :where conj '[?c :concept/type ?type]))

    (not-empty extra-where-attributes)
    (update :where into (map #(into ['?c] %) extra-where-attributes))

    true
    (update :where conj '[?c :concept/id])

    true
    (handle-deprecated deprecated include-deprecated)

    include-legacy-information
    (-> (update :find conj '?legacy-ams-id)
        (update :keys conj 'legacy-ams-id)
        (update :where conj '[(get-else $ ?c :concept.external-database.ams-taxonomy-67/id false) ?legacy-ams-id]))

    true
    remap-query))

(defn- add-legacy-id-to-concept [{:keys [legacy-ams-id] :as result}]
  (cond-> result legacy-ams-id (assoc-in [:concept :deprecated-legacy-id] legacy-ams-id)))

(defn find-concepts
  "Finds concepts in the database

  `args` is a map with the following keys (everything is optional):
  - `:id` (string) — concept id to search for
  - `:preferred-label` (string) — preferred label of a concept to search for
  - `:type` (coll of strings) — restrict concept's type to any of these
  - `:deprecated` (boolean) — restrict concepts to deprecated status (The deprecated param is deprecated)
  - `:include-deprecated` (boolean) — include deprecated concepts
  - `:related-ids` (coll of strings) and `:relation` (string) — restrict to relation to these ids
  - `:offset` (int) - pagination offset
  - `:limit` (int) — pagination limit
  - `:version` (int, `:latest` or `:next`) — db version to use, as per
    [[jobtech-taxonomy-api.db.database-connection/get-db]]. Defaults to `:latest`
  - `:extra-pull-fields` (pull pattern) - additional concept pull pattern
  - `:extra-where-attributes` (coll of attribute -> expected value entries) — additional query constraints
  - `:include-legacy-information` (boolean) — whether to include `:deprecated-legacy-id` to responses"
  [args]
  (-> (assoc args
             :db (db/get-db (:version args :latest))
             :pull-pattern (cond-> concept-pull-pattern
                             (:extra-pull-fields args) (into (:extra-pull-fields args))))
      make-find-concepts-query
      db/q
      (cond->> (:include-legacy-information args) (map add-legacy-id-to-concept))
      (->> (map :concept))
      sort-reply
      (->> (api-util/pagination (:offset args) (:limit args)))))

(defn exists? [db id]
  (boolean (seq (db/q '[:find ?e
                        :in $ ?id
                        :where [?e :concept/id ?id]]
                      db id))))

;; TODO FIX get db som innehåller unpublished sätt db innan! i args

(defn- split-on-pipe [string]
  (->> string
       (#(s/split % #"\|"))
       (remove s/blank?)
       (map #(s/trim %))
       (set)))

(defn- assert-concept-part [user-id {:keys [type definition preferred-label comment quality-level alternative-labels hidden-labels]}]
  (let [new-concept (cond-> {:concept/id (nano/generate-new-id-with-underscore)
                             :concept/type type
                             :concept/definition definition
                             :concept/preferred-label preferred-label}
                      quality-level (assoc :concept/quality-level quality-level)
                      alternative-labels (assoc :concept/alternative-labels (split-on-pipe alternative-labels))
                      hidden-labels (assoc :concept/hidden-labels (split-on-pipe hidden-labels)))
        tx [(api-util/user-action-tx user-id comment) new-concept]
        result (db/transact (db/get-conn) {:tx-data tx})
        _ (log/info result)]
    [result new-concept]))

(defn assert-concept "" [user-id {:keys [type preferred-label] :as params}]
  (let [existing (find-concepts {:preferred-label preferred-label :type type :version :next})]
    (if (> (count existing) 0)
      [false nil]
      (let [[result new-concept] (assert-concept-part user-id params)
            timestamp (if result (nth (first (:tx-data result)) 2) nil)]
        [result timestamp new-concept]))))

(defn assert-concept-handler [request]
  (let [query (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request request)
        [result timestamp new-concept] (assert-concept user-id query)
        _ (println new-concept)]
    (if result
      {:status 200 :body {:time timestamp :concept new-concept}}
      {:status 409 :body (types/map->nsmap {:error "Can't create new concept since it is in conflict with existing concept."})})))

(defn remove-label [concept-id label type user-id comment]
  {:pre [concept-id label type]}
  (let [result (db/transact
                (db/get-conn)
                {:tx-data [[:db/retract [:concept/id concept-id]
                            type label]
                           (api-util/user-action-tx user-id comment)]})]

    result))

(defn remove-alternative-label [user-id {:keys [id comment alternative-label]}]
  (remove-label id alternative-label :concept/alternative-labels user-id comment))

(defn remove-hidden-label [user-id {:keys [id comment hidden-label]}]
  (remove-label id hidden-label :concept/hidden-labels user-id comment))

(defn handle-remove-alternative-label [request]
  (let [query (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request request)
        result (remove-alternative-label user-id query)]
    (if result
      {:status 200 :body {:message "ok"}}
      {:status 409 :body (types/map->nsmap {:error "Can't remove alternative label."})})))

(defn handle-remove-hidden-label [request]
  (let [query (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request request)
        result (remove-hidden-label user-id query)]
    (if result
      {:status 200 :body {:message "ok"}}
      {:status 409 :body (types/map->nsmap {:error "Can't remove hidden label."})})))

;; CREATE RELATION

(defn fetch-relation-entity-id-from-concept-ids-and-relation-type [concept-1 concept-2 relation-type]
  (ffirst
   (db/q '{:find [?r]
           :in [$ ?id]
           :where [[?r :relation/id ?id]]}
         (db/get-db)
         (relation/id concept-1 relation-type concept-2))))

(defn- assert-relation-part [user-id comment c1 c2 type desc substitutability-percentage]
  (let [new-rel (relation/edge-tx c1 type c2
                                  (cond-> {}
                                    desc
                                    (assoc :relation/description desc)
                                    substitutability-percentage
                                    (assoc :relation/substitutability-percentage
                                           substitutability-percentage)))
        result (db/transact (db/get-conn)
                            {:tx-data [(api-util/user-action-tx user-id comment)
                                       new-rel]})]
    [result new-rel]))

(defn assert-relation "" [user-id comment concept-1 concept-2 type description substitutability-percentage]
  (if (fetch-relation-entity-id-from-concept-ids-and-relation-type concept-1 concept-2 type)
    [false nil]
    (assert-relation-part user-id comment concept-1 concept-2 type description substitutability-percentage)))

(defn assert-relation-query-params []
  (pu/build-parameter-map [:relation-type
                           :concept-1
                           :concept-2
                           :definition
                           :substitutability-percentage
                           :comment]))

(defn assert-relation-handler [request]
  (let [{:keys
         [relation-type
          definition
          concept-1
          concept-2
          substitutability-percentage
          comment]} (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request request)
        _ (log/info "POST /relation")
        [result new-relation]
        (assert-relation user-id comment
                         concept-1 concept-2 relation-type definition substitutability-percentage)]
    (if result
      {:status 200 :body (types/map->nsmap {:message "Created relation."})}
      {:status 409 :body (types/map->nsmap {:error "Can't create new relation since it is in conflict with existing relation."})})))

;; UPDATE RELATION

(defn- does-relation-exist? [relation-id]
  (= 1 (count
        (db/q '{:find [?r]
                :in [$ ?id]
                :where [[?r :relation/id ?id]]}
              (db/get-db)
              relation-id))))

(defn accumulate-relation
  "Updates a relation."
  [user-id
   {:keys [concept-1 concept-2 relation-type substitutability-percentage description comment]}]
  {:pre [user-id concept-1 concept-2 relation-type comment
         (does-relation-exist? (relation/id concept-1 relation-type concept-2))
         (not (every? nil? [substitutability-percentage description]))]}
  (let [relation (cond-> {:relation/id (relation/id concept-1 relation-type concept-2)}
                   substitutability-percentage
                   (assoc :relation/substitutability-percentage substitutability-percentage)
                   description
                   (assoc :relation/description description))
        result (db/transact
                (db/get-conn)
                {:tx-data [relation
                           (api-util/user-action-tx user-id comment)]})]
    (when result
      {:time (nth (first (:tx-data result)) 2) :relation relation})))

;; DELETE RELATION

(defn- retract-relation [user-id comment concept-1 concept-2 relation-type]
  (let [relation-entity-id (fetch-relation-entity-id-from-concept-ids-and-relation-type
                            concept-1
                            concept-2
                            relation-type)
        result (when relation-entity-id
                 (db/transact (db/get-conn) {:tx-data [[:db/retractEntity relation-entity-id]
                                                       (api-util/user-action-tx user-id comment)]}))]
    result))

(defn delete-relation-query-params []
  (pu/build-parameter-map [:relation-type :concept-1 :concept-2 :comment]))

(defn delete-relation-handler [request]
  (let [{:keys [comment relation-type concept-1 concept-2]} (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request request)
        _ (log/info (str "DELETE /relation " user-id " " relation-type " " concept-1 " " concept-2))
        result (retract-relation user-id comment concept-1 concept-2 relation-type)]
    (if result
      {:status 200 :body (types/map->nsmap {:message "Retracted relation."})}
      {:status 400 :body (types/map->nsmap {:error "Relation not found."})})))

(def fetch-simple-concept-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label])
    :in $ ?id
    :where [?e :concept/id ?id]])

(defn fetch-simple-concept [id]
  (ffirst (db/q fetch-simple-concept-query (db/get-db) id)))

;; Todo add case insensitive match unicode


(def duplicate-label-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label])
    :in $ ?preferred-label ?type
    :where
    [?e :concept/type ?type]
    [?e :concept/preferred-label ?preferred-label]])

(def duplicate-definition-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label])
    :in $ ?definition ?type
    :where
    [?e :concept/type ?type]
    [?e :concept/definition ?definition]])

(defn duplicate-concept-exists? [{:concept/keys [id preferred-label definition type]}]
  {:pre [id preferred-label definition type]}
  (let [concepts (concat (db/q duplicate-label-query (db/get-db) preferred-label type)
                         (db/q duplicate-definition-query (db/get-db) definition type))
        duplicates (filter #(not (= id (:concept/id (first %)))) concepts)]
    {:result (not (zero? (count duplicates)))
     :duplicates duplicates}))

(defn accumulate-concept
  "Updates concept.
  Will not permit two concepts with the same definition that already exists"
  [user-id {:keys [id type definition preferred-label
                   comment quality-level deprecated
                   alternative-labels hidden-labels]}]
  {:pre [id (not (every? nil?
                         [preferred-label definition
                          type quality-level deprecated
                          alternative-labels hidden-labels]))]}
  (let [old-concept (fetch-simple-concept id)
        concept (cond-> old-concept
                  preferred-label (assoc :concept/preferred-label preferred-label)
                  definition (assoc :concept/definition definition)
                  type (assoc :concept/type type)
                  quality-level (assoc :concept/quality-level quality-level)
                  (some? deprecated) (assoc :concept/deprecated deprecated)
                  alternative-labels (assoc :concept/alternative-labels
                                            (split-on-pipe alternative-labels))
                  hidden-labels (assoc :concept/hidden-labels
                                       (split-on-pipe hidden-labels)))

        duplicate-exists (duplicate-concept-exists? concept)
        datomic-result (when (not (:result duplicate-exists))
                         (db/transact (db/get-conn)
                                      {:tx-data [concept
                                                 (api-util/user-action-tx
                                                  user-id comment)]}))]
    (when datomic-result
      {:time (nth (first (:tx-data datomic-result)) 2) :concept concept})))

(comment
  ;; When we want to get typed data, use the dynamic pattern input
  ;; dynamic pattern input
  (db/q '[:find [(pull ?e pattern) ...]
          :in $ ?artist pattern
          :where [?e :release/artists ?artist]]
        db
        led-zeppelin
        [:release/name]))
