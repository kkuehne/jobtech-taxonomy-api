(ns jobtech-taxonomy-api.middleware
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [ring.middleware.session :as session]
            [jobtech-taxonomy-api.apikey :refer [apikey-backend]]
            [jobtech-taxonomy-api.authentication-service :as keymanager]
            [jobtech-taxonomy-api.env :refer [defaults]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.middleware.flash :refer [wrap-flash]]))

(defn on-error [request response]
  {:status 403
   :headers {}
   :body (str "Access to " (:uri request) " is not authorized")})

(defn wrap-restricted [handler]
  (restrict handler {:handler authenticated?
                     :on-error on-error}))

(defn authenticate-admin [api-key]
  (contains? (set (map first (filter (fn [[token role]] (= role :admin)) (keymanager/get-tokens-from-env))))
             (keyword api-key)))

;; Define an authfn, function with the responsibility
;; to authenticate the incoming token and return an
;; identity instance


(defn my-authfn
  [request token]
  (let [token (keyword token)]
    (keymanager/get-user token)))

;; Create an instance
(def api-backend-instance (apikey-backend {:authfn my-authfn}))

(defn wrap-auth [handler]
  (let [backend api-backend-instance]
    (-> handler
        (wrap-authentication backend)
        (wrap-authorization backend))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-auth
      wrap-flash
      (session/wrap-session)
      (wrap-defaults
       (-> site-defaults
           (assoc-in [:security :anti-forgery] false)
           (dissoc :session)))))
