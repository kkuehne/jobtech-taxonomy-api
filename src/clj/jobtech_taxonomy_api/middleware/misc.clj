(ns jobtech-taxonomy-api.middleware.misc
  (:require
   [clojure.string :as str]
   [clojure.tools.logging :as log]
   [metrics.core :refer [new-registry]]
   [metrics.counters :as metrics]))

(defn now []
  (new java.util.Date))

(def reg (new-registry))

;; Create a variable that keeps track of the number of parallel requests
(def concurrent-requests
  (metrics/counter reg "concurrent-requests"))

(defn log-request [handler]
  (fn [request]
    (let [timeStart (now)
          connections (metrics/value (metrics/inc! concurrent-requests))
          response (handler request)
          status (get response :status)
          api-key (get-in request [:headers "api-key"])
          referer (get-in request [:headers "referer"])
          from (get-in request [:headers "from"])
          req-str (get request :query-string)
          req-url (get request :uri)
          _ (metrics/dec! concurrent-requests)
          timeStop (now)
          requestTime (- (.getTime timeStop) (.getTime timeStart))
          _ (log/info
             (str "http " status ", " requestTime " ms, #" connections ", request: " req-url
                  (if req-str (str "?" req-str))
                  (if referer (str " referer: " referer) "")
                  (if from (str " from: " from) "")
                  (if api-key (str " api-key: " api-key) "")))]
      response)))

(defn trim-request-params [handler]
  (fn [request]
    (let [req (reduce
               #(update-in %1 [:query-params %2] str/trim)
               request
               (keys (get request :query-params)))]
      (handler req))))
