# JobTech Taxonomy API

The JobTech Taxonomy API is a REST API for the [JobTech Taxonomy
Database][0]. The JobTech Taxonomy Database contains terms or phrases used
at the Swedish labour market.

## Prerequisites

You will need to install:
1. For Windows users: [WSL][5]
2. [Clojure CLI][1] 1.10.3.855 or above
3. [AWS CLI][6]
4. [Java][7]
5. [Datomic CLI Tools][8]

[1]: https://clojure.org/guides/getting_started

You will also need a [Datomic][2] or a [datahike][4] database with the JobTech Taxonomy Database installed.
See <https://github.com/JobtechSwe/jobtech-taxonomy-database> how to create a Datomic database.

Set your connection details in `env/dev/resources/config.edn`.

For graphviz endpoint to work locally, you'll need to have graphviz installed locally, 
e.g. with `brew install graphviz` or `apt install graphviz`.

## Running

The project is based on [Luminus][3], which provides the web server
infrastructure.

You can either run Luminus from either your terminal or from a repl.

Regardless of how you start the project, don't forget to start the
Datomic proxy script first, in case you use Datomic Cloud:
```
$ ./test/scripts/datomic-socks-proxy tax-test-v4
https://docs.datomic.com/cloud/getting-started/connecting.html
```

### Controlling from Terminal

Change directory to the project root folder.

To start a web server for the application, run this
to connect to AWS Datomic:
```
clj -A:dev -X jobtech-taxonomy-api.core/start :port 3000
```

To use Datahike as backend, start the server like this:
```
DATABASE_BACKEND=datahike clj -A:dev -X jobtech-taxonomy-api.core/start :port 3000
```

### Controlling from REPL

Change to the project root folder and start your repl (If you use IntelliJ,
don't forget to load the project in your repl).

It is recommended to keep a file `dev-config.edn`, see "CREATE dev-config for local developement below".

To start the HTTP server and any other components such as databases,
run the start function in your repl:

    (start)

To run queries against the database:

```
(require '[jobtech-taxonomy-api.db.database-connection :as db])
(start)
(pprint (db/q '[:find (pull ?eid [*]) :in $ :where [?eid :concept/preferred-label "Personlig utveckling"]] (db/get-db)))
```

To run GraphQL queries:
```
(start)
(clojure.pprint/pprint (jobtech-taxonomy-api.db.graphql/execute "{ concepts(type: \"wage-type\") { id preferred_label }}" nil nil nil))
```

### Docker

Build the docker image:
```
docker build . -t api
```

Run the API locally using docker with Datomic as backend, run:
```
docker run -p 3000:3000 --name api --rm api
```

Run the API locally using docker with Datahike as backend, with the database in the folder datahike-file-db, run:
```
docker run -v $PWD/datahike-file-db:/datahike-file-db:Z -e DATABASE_BACKEND=datahike -p 3000:3000 --name api --rm api
```

Stop the API in another terminal, run:
```
docker container stop api
```

## Next step

Then open the following URL in a web browser:

    http://localhost:3000/v1/taxonomy/swagger-ui/index.html

## Authorize

Authorization is only needed to read unpublished taxonomy versions and for DB write access.
To get write access, click the Authorize button, and enter your account code,
defined in `env/dev/resources/config.edn`.

## Running a query

    curl -X GET --header 'Accept: application/json' 'http://localhost:3000/v1/taxonomy/main/concepts?preferred-label=Danska'

## Testing
The integration test setup creates a temporary database for each test,
which makes it safe to do any modifications without leaving traces
behind.

Summary:
 - test runner: Kaocha (https://github.com/lambdaisland/kaocha).
 - test command: `clj -A:test -M:kaocha --focus-meta TAG`
   where TAG is the name of one of the test's tags (such as `integration`).
 - status: for integration tests that rely on a live database, only one test
   can be run at a time. This means that you should assign each test a unique
   tag (e.g. `(test/deftest ^:changes-test-2 changes-test-2 ...)`), and then
   run it with `clj -M:test --focus-meta changes-test-2`.

### Run kaocha tests

```
clj -M:test:kaocha
```

### Running code coverage

The code coverage is based on kaocha-cloverage. Run with:
```
clj -M:test:kaocha --plugin cloverage
```

### Testing in the REPL

Start the socks-proxy in the first terminal:
```
while [ 1 ] ; do ./datomic-socks-proxy prod-jobtech-taxonomy-db ; done
```
Run the following commands in another terminal:
```shell
$ clj -A:dev:test
user=> (use 'kaocha.repl)
user=> (run 'jobtech-taxonomy-api.test.graphql-test/graphql-test-1)
```

### nREPL

If you are using nrepl, you can start it like so:
```shell
$ clj -A:dev:test -M -m nrepl.cmdline
```

Alias `:dev` is required for local development, and `:test` is also useful for creating tests

Typical emacs/cider launch command can look like this:
```shell
 /usr/local/bin/clojure \
 -Sdeps '{:deps {nrepl/nrepl {:mvn/version "0.8.3"} refactor-nrepl/refactor-nrepl {:mvn/version "2.5.1"} cider/cider-nrepl {:mvn/version "0.26.0"}} :aliases {:cider/nrepl {:main-opts ["-m" "nrepl.cmdline" "--middleware" "[refactor-nrepl.middleware/wrap-refactor,cider.nrepl/cider-middleware]"]}}}' \
 -A:dev:test \
 -M:cider/nrepl
```

Connect to a running nREPL (on port 7000):
```
clj -R:nREPL -m nrepl.cmdline -c -p 7000
```

### How to write an integration test

#### File and namespace
Your test should reside in the directory `test/clj/jobtech_taxonomy_api/test/`.

You should either pick an existing file, or create a new file, ending
with `_test.clj`.  It should use a namespace like this: `(ns
jobtech-taxonomy-api.test.FILENAME ...)`, where FILENAME is for example
`changes-test`.

You need to require `[jobtech-taxonomy-api.test.test-utils :as util]`.

#### Define fixtures
Place one occurance of this line in your test file:
`(test/use-fixtures :each util/fixture)`.

#### Define a test which calls functions directly
Here is a simple example of a test which asserts a skill concept, and
then checks for its existence.

First, require
```
[jobtech-taxonomy-api.db.concept :as c]

```

Then write a test:
```
(test/deftest ^:concept-test-0 concept-test-0
  (test/testing "Test concept assertion."
    (c/assert-concept "skill" "cykla" "cykla")
    (let [found-concept (first (core/find-concept-by-preferred-term "cykla"))]
      (test/is (= "cykla" (get found-concept :preferred-label))))))
```

#### Define a test which calls the Luminus REST API
Here is a simple example of a test which asserts a skill concept, and
then checks for its existence via the REST API:

First, require
```
[jobtech-taxonomy-api.db.concept :as c]

```

Then write a test:
```
(test/deftest ^:changes-test-1 changes-test-1
  (test/testing "test event stream"
    (c/assert-concept "skill" "cykla" "cykla")
    (let [[status body] (util/send-request-to-json-service
                         :get "/v0/taxonomy/public/concepts"
                         :query-params [{:key "preferred-label", :val "cykla"}])]
      (test/is (= "cykla" (get (first body) :preferred-label))))))
```

## Deployment
The deployment of this is handled by [jobtech-taxonomy-api-gitops](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops).

In case the build pipeline fails in OpenShift, images can be pushed manually to Nexus.
```
git checkout 8f815df2047da934d13f724f0d3c0623db5f57d9
docker build . -t api
docker tag api:latest docker-images.jobtechdev.se/batfish/jobtech-taxonomy-api:8f815df2047da934d13f724f0d3c0623db5f57d9
docker push docker-images.jobtechdev.se/batfish/jobtech-taxonomy-api:8f815df2047da934d13f724f0d3c0623db5f57d9
```

## Logging

By default, logging functionality is provided by the
clojure.tools.logging library. The library provides macros that
delegate to a specific logging implementation. The default
implementation used in Luminus is the logback library.

Any Clojure data structures can be logged directly.


### Examples
```
(ns example
 (:require [clojure.tools.logging :as log]))

(log/info "Hello")
=>[2015-12-24 09:04:25,711][INFO][myapp.handler] Hello

(log/debug {:user {:id "Anonymous"}})
=>[2015-12-24 09:04:25,711][DEBUG][myapp.handler] {:user {:id "Anonymous"}}
```

### Logging of exceptions

```
(ns example
 (:require [clojure.tools.logging :as log]))

(log/error (Exception. "I'm an error") "something bad happened")
=>[2015-12-24 09:43:47,193][ERROR][myapp.handler] something bad happened
  java.lang.Exception: I'm an error
    	at myapp.handler$init.invoke(handler.clj:21)
    	at myapp.core$start_http_server.invoke(core.clj:44)
    	at myapp.core$start_app.invoke(core.clj:61)
    	...
```

### Logging backends
### Configuring logging
Each profile has its own log configuration. For example, `dev`'s
configuration is located in `env/dev/resources/logback.xml`.

It works like a standard Java log configuration, with appenders and loggers.

The default configuration logs to standard out, and to log files in log/.

## License

EPL-2.0

Copyright © 2019 Jobtech

## CREATE dev-config for local developement
Create the file "dev-config.edn" with this content

```
;; WARNING
;; The dev-config.edn file is used for local environment variables, such as database credentials.
;; This file is listed in .gitignore and will be excluded from version control by Git.

{:dev true
 :port 3000
 ;; when :nrepl-port is set the application starts the nREPL server on load
 :nrepl-port 7000

 ; set your dev database connection URL here
 ; :database-url "datomic:free://localhost:4334/jobtech_taxonomy_api_dev"

 ; alternatively, you can use the datomic mem db for development:
 ; :database-url "datomic:mem://jobtech_taxonomy_api_datomic_dev"
}
```

## Adding auto code formatting as a pre-commit hook

Run this command to configure git to look for hooks in .githooks/:
`git config --local core.hooksPath .githooks/`

## COMMON ERRORS

If you get :server-type must be :cloud, :peer-server, or :local
you have forgot to start luminus. Run (start) in the user> namespace


# Contact Information

Bug reports are issued at [https://gitlab.com/team-batfish/jobtech-taxonomy-api/issues][the repo on GitLab].

Questions about the Taxonomy database, about Jobtech, about the API in
general are best emailed to [jobtechdev@arbetsformedlingen.se][Jobtechdev contact
email adress].

Check out our other open APIs at [jobtechdev][Jobtechdev].


[0]: https://github.com/JobtechSwe/jobtech-taxonomy-database "The JobTech Taxonomy Database"
[1]: https://clojure.org/guides/getting_started "Clojure CLI"
[2]: https://www.datomic.com "Datomic"
[3]: http://www.luminusweb.net "Luminus"
[4]: https://datahike.io "datahike"
[5]: https://docs.microsoft.com/en-us/windows/wsl/install-manual "WSL"
[6]: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html "AWS CLI"
[7]: https://openjdk.java.net/ "Java"
[8]: https://docs.datomic.com/cloud/operation/cli-tools.html#cloud "Datomic CLI Tools"
