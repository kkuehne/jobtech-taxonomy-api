#!/bin/sh

# Runs a simple performance test using curl
# E.g. start the Taxonomy API Server and run this command
# $ ./test/performance/run-perf.sh localhost:3000
# or test the U1 environment in OpenShift
# $ ./test/performance/run-perf.sh http://api-u1-taxonomy-api-gitops.test.services.jtech.se

if [ -z "$1" ] ; then
  echo "API endpoint missing"
  exit 1
fi

EP=$1
KEY=perf

test_query()
{
  Q=$1
  echo $Q
  RESP=$(time (curl -X GET "$EP/v1/taxonomy/$Q" -H "accept: application/json" -H "api-key: $KEY" > /dev/null 2>&1))
}

run_once()
{
  # Testing the autocomplete endpoint
  test_query "suggesters/autocomplete?query-string=sve&type=language&limit=6"
  test_query "suggesters/autocomplete?query-string=disk&type=skill&limit=6"
  test_query "suggesters/autocomplete?query-string=disk&type=ssyk-level-4&limit=6"
  test_query "suggesters/autocomplete?query-string=disk&type=occupation-name&limit=6"
  test_query "suggesters/autocomplete?query-string=bor&type=region&relation=narrower&related-ids=i46j_HmG_v64&limit=6"
  test_query "suggesters/autocomplete?query-string=disk&type=muncipality&limit=6"
  test_query "suggesters/autocomplete?query-string=sve&type=country&limit=6"

  # Testing the main concepts endpoint
  test_query "main/concepts?related-ids=kLyY_rwr_aJr&type=occupation-name&version=1&relation=narrower"
  test_query "main/concepts?related-ids=fRwb_quG_Hmx&type=ssyk-level-4&version=1&relation=broader"
  test_query "main/concepts?id=tHZB_LJU_8LG"
  test_query "main/concepts?related-ids=SFZw_QVx_MU7&type=ssyk-level-4&version=1&relation=broader"
  test_query "main/concepts?type=employment-type"
  test_query  "main/concepts?related-ids=7pz9_eY9_D2s&type=skill&relation=related"

  # Test the GraphQL endpoint

  # { concepts(type: "skill" include_deprecated: true limit: 10 offset: 0) { preferred_label id }}
  test_query "graphql?query=%7B%20concepts%28type%3A%20%22skill%22%20include_deprecated%3A%20true%20limit%3A%2010%20offset%3A%200%29%20%7B%20preferred_label%20id%20%7D%7D"

  # { concepts(id: "QbTy_45b_cTU") { definition type label: preferred_label substitutes(type: "occupation-name") { id type label: preferred_label percentage: substitutability_percentage } keywords: related(type: "keyword") { id type label: preferred_label } isco_codes: broader(type: "isco-level-4") { code: isco_code_08 label: preferred_label } } }
  test_query "graphql?query=%7B%20concepts%28id%3A%20%22QbTy_45b_cTU%22%29%20%7B%20definition%20type%20label%3A%20preferred_label%20substitutes%28type%3A%20%22occupation-name%22%29%20%7B%20id%20type%20label%3A%20preferred_label%20percentage%3A%20substitutability_percentage%20%7D%20keywords%3A%20related%28type%3A%20%22keyword%22%29%20%7B%20id%20type%20label%3A%20preferred_label%20%7D%20isco_codes%3A%20broader%28type%3A%20%22isco-level-4%22%29%20%7B%20code%3A%20isco_code_08%20label%3A%20preferred_label%20%7D%20%7D%20%7D"

  # { concepts(id: "Y6yY_SuR_hVh") { preferred_label id related { id preferred_label type broader(type: "skill-headline") { preferred_label } } } }
  test_query "graphql?query=%7B%20concepts%28id%3A%20%22Y6yY_SuR_hVh%22%29%20%7B%20preferred_label%20id%20related%20%7B%20id%20preferred_label%20type%20broader%28type%3A%20%22skill-headline%22%29%20%7B%20preferred_label%20%7D%20%7D%20%7D%20%7D"

  # { concepts(type: "wage-type") { preferred_label id } }
  test_query "graphql?query=%7B%20concepts%28type%3A%20%22wage-type%22%20include_deprecated%3A%20true%29%20%7B%20preferred_label%20id%20%7D%20%7D"

  # { concepts(type: "wage-type") { preferred_label id } }
  test_query "graphql?query=%7B%20concepts%28type%3A%20%22wage-type%22%29%20%7B%20preferred_label%20id%20%7D%20%7D"
}

run_many()
{
  for i in {1..10} ; do
    res=$(run_once 2>&1 | grep ^real | sed 's/.*0m//g ; s/s$//g')
    echo $res
  done
}

# Test that the server is answering
TMPFILE=$(mktemp)
wget -q $EP/v1/taxonomy/main/versions --header "accept: application/json" --header "api-key: $KEY" -i $TMPFILE
w=$? # Get return value
rm $TMPFILE

# Check if the tests should be run
if [ "$w" != "0" ] ; then
  echo "The server does not respond"
else
  # Run all tests many times and print the average response time
  run_many | awk '{for(i=1;i<=NF;i++) arr[i]+=$i} END {for(i=1;i<=NF;i++) print arr[i]/NR}'
fi
