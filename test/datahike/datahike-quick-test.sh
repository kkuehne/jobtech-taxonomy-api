#!/bin/bash

#
# Run this script from a folder that contains folders with git checkouts of
# jobtech-taxonomy-api and wanderung.
#

# Exit on any errors
set -e
# use better pattern matching
shopt -s extglob

# default values
USE_TESTS=false
MIGRATION_SOURCE=../jobtech-taxonomy-api/test/datahike/datomic-cloud.edn
MIGRATION_TARGET=../jobtech-taxonomy-api/test/datahike/datahike-file.edn
TARGET_DIR=./
GRAPHQL_QUERY="{ concepts(type: \"skill\") { preferred_label id } }\n  "
ENDPOINTS="/main/versions"
QUICK_TEST=false
MIGRATE_ALWAYS=false

setup_dir() {
    # Make a temporary directory
    echo "Setting up temp dir ..."
    DIR=$(mktemp -d $TARGET_DIR/datahike-test.XXXXXXXXXXXXX)
    cd $DIR
    pwd
    FULL_DIR=$( pwd )
    DATAHIKE_DIR="$FULL_DIR/dh"
}

clone_repos() {
    git clone --single-branch https://github.com/lambdaforge/wanderung.git
    git clone --single-branch https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
}

run_kaocha_tests() {
    echo "Runnning kaocha tests ..."
    cd jobtech-taxonomy-api
    DATABASE_BACKEND=datahike time -o ../kaocho_time.txt clj -M:test:kaocha
    cd -
    echo "Done"
}

migrate_db() {
    echo "Migrating DB ..."
    ./jobtech-taxonomy-api/test/scripts/datomic-socks-proxy -p jobtech -r eu-central-1 tax-test-v4 &
    datomic=$!
    rm -rf $DATAHIKE_DIR
    cd wanderung
    cp $MIGRATION_SOURCE .
    cp $MIGRATION_TARGET .
    DATAHIKE_STORE_PATH=$DATAHIKE_DIR time -o ../migration_time.txt clj -M:run-m -s "$(basename -- $MIGRATION_SOURCE)" -t "$(basename -- $MIGRATION_TARGET)"
    cd -
    echo "Done"
    echo "Disconnecting from datomic ..."
    sleep 100
    kill -9 $datomic
    echo "Done"
}

start_api() {
    echo "Starting the API ..."
    cd jobtech-taxonomy-api
    DATAHIKE_CFG__STORE__PATH=$DATAHIKE_DIR \
                             DATABASE_BACKEND=datahike \
                             clj -A:dev -X jobtech-taxonomy-api.core/start :port 3000 &
    api=$!
    cd -
    sleep 100 # wait for the API to come up
    echo "Done"
}

test_api() {
    echo "Testing API ..."
    EP="http://localhost:3000/v1/taxonomy"
    HEADER='--header=accept: application/json'
    wget "$HEADER" "$EP/status/health"
    wget "$HEADER" "$EP/status/ready"
    wget "$HEADER" "$EP/main/concepts?offset=1&limit=10&type=skill"
    # check that the last version is available
    # is the time variable?
    curl -s -X GET "$EP/main/versions" -H  "accept: application/json" | grep 2021-10-29T14:00:04.942Z
    # test some graphql queries
    wget "$HEADER" "$EP/graphql?query=%7B%20%20%20concepts%28type%3A%20%5B%22keyword%22%2C%20%22occupation-name%22%5D%2C%20preferred_label_contains%3A%20%22dag%22%29%20%7B%20%20%20%20%20id%20%20%20%20%20type%20%20%20%20%20preferred_label%20%20%20%20%20related%20%7B%20%20%20%20%20%20%20id%20%20%20%20%20%20%20preferred_label%20%20%20%20%20%7D%20%20%20%7D%20%7D"
    wget "$HEADER" "$EP/graphql?query=%7B%20%20%20concepts%28type%3A%20%5B%22keyword%22%2C%20%22occupation-name%22%5D%2C%20preferred_label_contains%3A%20%22dag%22%29%20%7B%20%20%20%20%20id%20%20%20%20%20type%20%20%20%20%20preferred_label%20%20%20%7D%20%7D"
    wget "$HEADER" "$EP/graphql?query=%7B%20%20%20concepts%28type%3A%20%22wage-type%22%29%20%7B%20%20%20%20%20id%20%20%20%20%20preferred_label%20%20%20%7D%20%7D"
    wget "$HEADER" "$EP/graphql?query=query%20MyQuery%20%7B%20%20%20changelog%28type%3A%20%22skill%22%2C%20from%3A%207%2C%20to%3A%208%29%20%7B%20%20%20%20%20event%3A%20__typename%20%20%20%20%20timestamp%20%20%20%20%20user%20%20%20%20%20comment%20%20%20%20%20concept%20%7B%20%20%20%20%20%20%20id%20%20%20%20%20%20%20preferred_label%20%20%20%20%20%7D%20%20%20%20%20...%20on%20Updated%20%7B%20%20%20%20%20%20%20changes%20%7B%20%20%20%20%20%20%20%20%20attribute%20%20%20%20%20%20%20%20%20old_value%20%20%20%20%20%20%20%20%20new_value%20%20%20%20%20%20%20%7D%20%20%20%20%20%7D%20%20%20%7D%20%7D"
    # check that all types are there
    curl -s -X GET "$EP/main/concept/types" -H  "accept: application/json" | jq | wc -l | grep 46
    echo "Done"

    echo "Testing the graphql performance..."
    for query in "${GRAPHQL_QUERY[@]}"; do
        if [ -n "${query##+([[:space:]])}" ];
        then
            for i in {1..10} ; do
                echo $query
                curl -vvv -G \
                     --data-urlencode "query=$query" \
                     "$EP/graphql" -H "accept: application/json"
            done
        fi
    done
    echo "Done"

    echo "Testing the endpoint performance..."
    for endpoint in $ENDPOINTS; do
        for i in {1..10} ; do
            time -o ../endpoint_time.txt curl -X GET "$EP$endpoint" -H  "accept: application/json"
        done
    done
    echo "Done"
    }

stop_api() {
    echo "Stopping the API"
    kill -9 $api
}

help() {
    echo "Usage:  $(basename $0) [ -d | --dir target directory]
               [ -k | --kaocha run kaocha tests]
               [ -s | --source path of migration source configuration]
               [ -t | --target path of migration target configuration]
               [ -g | --graphql path of graphql test queries]
               [ -e | --endpoints path of endpoint test queries]
               [ -q | --quick run a quick test without benchmarks]
               [ -m | --migrate test migration on quick test]
               [ -h | --help  ]"
    exit 2
}

SHORT=d:,s:,t:,k,g:,e:,q,m,h
LONG=dir:,source:,target:,kaocha,graphql:,endpoints:,quick,migrate,help
OPTS=$(getopt -a -n bench --options $SHORT --longoptions $LONG -- "$@")

VALID_ARGUMENTS=$# # Returns the count of arguments that are in short or long options

eval set -- "$OPTS"

while :
      do
          case "$1" in
              -d | --dir )
                  TARGET_DIR="$2"
                  if [ ! -d "$TARGET_DIR" ]; then
                      echo "Target directory does not exist. Creating it ..."
                      mkdir -p $TARGET_DIR
                      echo "Done"
                  fi
                  shift 2
                  ;;
              -k | --kaocha )
                  USE_TESTS=true
                  shift
                  ;;
              -h | --help )
                  help
                  ;;
              -s | --source )
                  MIGRATION_SOURCE="$(readlink -m $2)"
                  shift 2
                  ;;
              -t | --target )
                  MIGRATION_TARGET="$(readlink -m $2)"
                  shift 2
                  ;;
              -g | --graphql )
                  str=$(cat $2)
                  readarray -d --- -t GRAPHQL_QUERY<<< "$str"
                  shift 2
                  ;;
              -e | --endpoints )
                  ENDPOINTS="$(cat $2)"
                  shift 2
                  ;;
              -q | --quick )
                  QUICK_TEST=true
                  shift
                  ;;
              -m | --migrate )
                  MIGRATE_ALWAYS=true
                  shift
                  ;;
              --)
                  shift;
                  break
                  ;;
              *)
                  echo "Unexpected option: $1"
                  help
                  ;;
          esac
done

# setup directory and repos
setup_dir
clone_repos

# run tests
if [ $USE_TESTS = true ]
then
    run_kaocha_tests
else
    echo "Skipping kaocha tests ..."
fi

# migrate data
if [ $MIGRATE_ALWAYS = true ] && [ $QUICK_TEST = true ]
then
    migrate_db
fi

# test system
if [ $QUICK_TEST = true ]
then
    echo "Skipping benchmarks ..."
else
    migrate_db
    start_api
    test_api
    stop_api
fi

echo "Done"
