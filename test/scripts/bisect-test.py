#!/usr/bin/env python3

"""
This script can be used when running git bisect against a running server.
Start by copying the script to the root folder. Otherwise bisect will not find it when checking out an
older version.
cp test/scripts/bisect-test.py .
cp test/scripts/local_server.py .

Start a bisect run with:
git bisect start <failing commit> <passing commit>
   e.g. git bisect start 5a2f5e0bbbcee298ad73666eecb4a4f8d1214cf8 69aa1a887629d59dcfb8f406262483276d3b4b41
git bisect run ./bisect-test.py
git bisect reset # quit the bisect session

Don't use this script if the error can be triggered with the kaocha test.
"""

import sys
from local_server import start_server, stop_server, run_query

# Config test
test_endpoint = "http://localhost:3000/v1/taxonomy/main/versions"
api_key = "111"

# If set to false git bisect will ignore this version if the server does not start
config_build_fail_is_failure = False

# Set the backend to test with
backend = "datomic"

def run_test():
    res = start_server(config_build_fail_is_failure, backend)
    if res == 0:
        # Run tests
        print("Server up, run test")
        res = run_query(test_endpoint, api_key)

    print("Stopping server")
    stop_server()
    return res

res = run_test()
sys.exit(res)
