import sys
from subprocess import run, PIPE

def patch():
        print("patch")
        returnCode = 0
        try:
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/def\ db-backend\ :datahike/def\ db-backend\ :datomic/",
                    "src/clj/jobtech_taxonomy_api/config.clj",
                ],
                stdout=PIPE,
            )
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/sendandi.api/datomic.client.api/ ; s/wanderung.db.api/datomic.client.api/",
                    "src/clj/jobtech_taxonomy_api/db/database_connection.clj",
                ],
                stdout=PIPE,
            )
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/.*datahike-client.*datahike-cfg.*// ; s/;\ (d\/client/(d\/client/",
                    "src/clj/jobtech_taxonomy_api/db/database_connection.clj",
                ],
                stdout=PIPE,
            )
            returnCode = res.returncode
        except CalledProcessError as e:
            output = e.output
            returncode = e.returncode

        return 0

def patch2(sha):
        print("patching config and database_connection")
        try:
            run(
                [ "git"
                , "checkout", str(sha).strip(), "--"
                , "src/clj/jobtech_taxonomy_api/config.clj"
                , "src/clj/jobtech_taxonomy_api/db/database_connection.clj"
                ],
                stdout=PIPE,
            )
        except CalledProcessError as e:
            print("Exception:", e)
            sys.exit(1)

def patch3(sha):
        print("patching deps.edn")
        try:
            run(
                [ "sed"
                , "-i.old"
                , "/datahike/s/sha.*/sha \"" + sha + "\"/"
                , "deps.edn"
                ],
                stdout=PIPE,
            )
        except CalledProcessError as e:
            print("Exception:", e)
            sys.exit(1)

def unpatch():
        print("unpatch")
        try:
            run(
                [ "git", "reset", "--hard", "HEAD" ],
                stdout=PIPE,
            )
        except CalledProcessError as e:
            print("Exception:", e)
            sys.exit(1)
