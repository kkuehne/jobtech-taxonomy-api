#!/usr/bin/env python3

"""
This script runs a performance test on each commit in the Datahike git log.

Run the script from the root folder.
./test/scripts/perf_test_datahike.py .
"""

import sys
import time
from subprocess import run, PIPE
from local_server import start_server, stop_server, run_query
from patch_backend import patch3, unpatch

# Config Datahike repo
datahike_repo = "/Users/clajo/Repos/JobTech/replicative/datahike"
datahike_start_version = "v.0.4.0"

# Config test
api_key = "111"

# If set to false git bisect will ignore this version if the server does not start
config_build_fail_is_failure = False

# Set the backend to test with
backend = "datahike"

def measure(f):
    startTime = time.time()
    f();
    return time.time() - startTime

def run_performance_test():
    eps = [ "suggesters/autocomplete?query-string=sve&type=language&limit=6"
          ]
    host_url = "http://localhost:3000/v1/taxonomy/"

    total = 0
    for i in eps:
        total += measure(lambda: run_query(host_url + i, api_key))
    return total

def run_single_test(sha):
    res = start_server(config_build_fail_is_failure, backend, False)
    if res == 0:
        print("Server up")
        # Run tests
        print("Run test")
        t = run_performance_test()
        print("SHA:", sha, "time:", t)

    print("Stopping server")
    stop_server()
    return res

def get_sha(path = "."):
    sha = ""
    try:
        proc = run(
            [ "git", "-C", path, "rev-parse", "HEAD" ],
            stdout=PIPE,
        )
        sha = proc.stdout.decode("utf-8").rstrip('\n\r')
    except CalledProcessError as e:
        print("Exception:", e)
        sys.exit(1)
    return sha

def checkout_start():
    try:
        proc = run(
            [ "git", "-C", datahike_repo, "checkout", "-q", datahike_start_version ],
            stdout=PIPE,
        )
    except CalledProcessError as e:
        print("Exception:", e)
        sys.exit(2)

def checkout_previous(path = "."):
    try:
        proc = run(
            [ "git", "-C", path, "checkout", "-q", "HEAD^" ],
            stdout=PIPE,
        )
    except CalledProcessError as e:
        print("Exception:", e)
        sys.exit(2)

def iterate_history():
    nbr = 0
    checkout_start()
    sha = get_sha(datahike_repo)
    while True:
        nbr += 1
        print("test:", nbr)
        run_single_test(sha)
        unpatch()
        checkout_previous(datahike_repo)
        sha = get_sha(datahike_repo)
        patch3(sha)

iterate_history()
