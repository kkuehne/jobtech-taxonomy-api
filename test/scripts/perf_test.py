#!/usr/bin/env python3

"""
This script runs a performance test on each commit in the git log.

Run the script from the root folder.
./test/scripts/perf_test.py .
"""

import sys
import time
from subprocess import run, PIPE
from local_server import start_server, stop_server, run_query
from patch_backend import patch2, unpatch

# Config test
api_key = "111"

# If set to false git bisect will ignore this version if the server does not start
config_build_fail_is_failure = False

# Set the backend to test with
backend = "datahike"

def measure(f):
    startTime = time.time()
    f();
    return time.time() - startTime

def run_performance_test():
    eps = [ "suggesters/autocomplete?query-string=sve&type=language&limit=6"
          , "suggesters/autocomplete?query-string=disk&type=skill&limit=6"
          , "suggesters/autocomplete?query-string=disk&type=ssyk-level-4&limit=6"
          #, "suggesters/autocomplete?query-string=disk&type=occupation-name&limit=6"
          #, "suggesters/autocomplete?query-string=bor&type=region&relation=narrower&related-ids=i46j_HmG_v64&limit=6"
          #, "suggesters/autocomplete?query-string=disk&type=muncipality&limit=6"
          #, "suggesters/autocomplete?query-string=sve&type=country&limit=6"
          ]
    host_url = "http://localhost:3000/v1/taxonomy/"

    total = 0
    for i in eps:
        total += measure(lambda: run_query(host_url + i, api_key))
    return total

def run_single_test(sha):
    res = start_server(config_build_fail_is_failure, backend)
    if res == 0:
        # Run tests
        print("Server up, run test")
        t = run_performance_test()
        print("SHA:", sha, "time:", t)

    print("Stopping server")
    stop_server()
    return res

def get_sha():
    sha = ""
    try:
        proc = run(
            [ "git", "rev-parse", "HEAD" ],
            stdout=PIPE,
        )
        sha = proc.stdout.decode("utf-8").rstrip('\n\r')
    except CalledProcessError as e:
        print("Exception:", e)
        sys.exit(1)
    return sha

def checkout_previous():
    try:
        proc = run(
            [ "git", "checkout", "-q", "HEAD^" ],
            stdout=PIPE,
        )
    except CalledProcessError as e:
        print("Exception:", e)
        sys.exit(2)

def iterate_history():
    nbr = 0
    sha = get_sha()
    base = sha
    while True:
        nbr += 1
        print("test:", nbr)
        run_single_test(sha)
        unpatch()
        checkout_previous()
        sha = get_sha()
        patch2(base)

iterate_history()
