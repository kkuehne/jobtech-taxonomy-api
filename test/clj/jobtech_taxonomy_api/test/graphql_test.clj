(ns ^:integration-graphql-tests jobtech-taxonomy-api.test.graphql-test
  (:require [clojure.test :as test]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.core :as db.core]
            [jobtech-taxonomy-api.db.daynotes :as daynotes]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.test.db.database :as db]
            [jobtech-taxonomy-api.test.test-utils :as util]))

(test/use-fixtures :each util/fixture)

(def ^:private broaderSkill {:type "skill" :definition "Drive machinery" :preferred-label "drive"})
(def ^:private narrowerSkill1 {:type "skill" :definition "ride bikes" :preferred-label "bike"})
(def ^:private narrowerSkill2 {:type "skill" :definition "fly airplanes" :preferred-label "fly"})

(def ^:private userid "Stallman")
(def ^:private endpoint "/v1/taxonomy/graphql")

(def schemaQuery
  (str
   "query IntrospectionQuery {"
   "__schema {"
   "queryType { name }"
   "}}"))

(test/deftest ^:integration-graphql-test-0 access-schema-without-auth
  (test/testing "test that the schema is accessible without authentication"
    (let [[status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val schemaQuery}])]
      (test/is (= 200 status)))))

(test/deftest ^:integration-graphql-test-1 graphql-test-1
  (test/testing "test simple graphql query"
    (let [_ (db/create-version-0)
          [_ _ new-concept] (concepts/assert-concept userid broaderSkill)
          query (str "{concepts(id: \"" (:concept/id new-concept) "\") {preferred_label}}")
          _ (versions/create-new-version 1)
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "drive" preferred_label)))))

(defn- buildRelationQuery
  [relation id id1 id2]
  (str
   "{concepts(id: \"" id "\") {"
   relation "(id: [\"" id1 "\" \"" id2 "\"]) {"
   "preferred_label"
   "}}}"))

(defn- test-relation
  [relation]
  (let [_ (db/create-version-0)
        [_ _ broaderConcept] (concepts/assert-concept userid broaderSkill)
        [_ _ narrowerConcept1] (concepts/assert-concept userid narrowerSkill1)
        [_ _ narrowerConcept2] (concepts/assert-concept userid narrowerSkill2)
        id (:concept/id broaderConcept)
        id1 (:concept/id narrowerConcept1)
        id2 (:concept/id narrowerConcept2)
        _ (concepts/assert-relation
           userid nil id id1 relation "desc" 0)
        _ (concepts/assert-relation
           userid nil id id2 relation "desc" 0)
        _ (versions/create-new-version 1)
        query (buildRelationQuery relation id id1 id2)
        [status body] (util/send-request-to-json-service
                       :get endpoint
                       :query-params [{:key "query", :val query}])
        rel (case relation
              "narrower" :narrower
              "broader" :broader
              "related" :related
              (throw (Exception. (str "'" relation "' not supported in test-relation"))))
        preferred_label1 (get-in body [:data :concepts 0 rel 0 :preferred_label])
        preferred_label2 (get-in body [:data :concepts 0 rel 1 :preferred_label])]
    (test/is (= 200 status))
    (test/is (= ["bike" "fly"] (sort [preferred_label1 preferred_label2])))))

(test/deftest ^:integration-graphql-test-2 graphql-test-2
  (test/testing "test a 'broader' graphql query"
    (test-relation "broader")))

(test/deftest ^:integration-graphql-test-3 graphql-test-3
  (test/testing "test a 'related' graphql query"
    (test-relation "related")))

(test/deftest ^:integration-graphql-test-4 graphql-test-4
  (test/testing "test adding a broader relation and find it by a narrower graphql query"
    (let [_ (db/create-version-0)
          [_ _ broaderConcept] (concepts/assert-concept userid broaderSkill)
          [_ _ narrowerConcept] (concepts/assert-concept userid narrowerSkill1)
          id (:concept/id broaderConcept)
          id1 (:concept/id narrowerConcept)
          _ (concepts/assert-relation
             userid nil id1 id "broader" "desc" 0)
          _ (versions/create-new-version 1)
          query (buildRelationQuery "narrower" id id1 "incorrect conceptID")
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :narrower 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "bike" preferred_label)))))

(test/deftest ^:integration-graphql-test-5 graphql-test-5
  (test/testing "changing concept between versions is observable with versions query"
    (let [_ (db/create-version-0)
          [_ _ {:concept/keys [id]}] (concepts/assert-concept
                                      userid
                                      {:type "skill"
                                       :definition "concept v1"
                                       :preferred-label "concept v1"})
          _ (versions/create-new-version 1)
          _ (concepts/accumulate-concept
             userid
             {:id id
              :definition "concept v2"
              :preferred-label "concept v2"})
          _ (versions/create-new-version 2)
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query"
                                         :val (format "
                                                 {
                                                   versions {
                                                     id
                                                     concepts(id: \"%s\") {
                                                       id
                                                       definition
                                                       preferred_label
                                                     }
                                                   }
                                                   }" id)}])]
      (test/is (= 200 status))
      (test/is (= {:data
                   {:versions
                    [{:id 1
                      :concepts [{:id id
                                  :definition "concept v1"
                                  :preferred_label "concept v1"}]}
                     {:id 2
                      :concepts [{:id id
                                  :definition "concept v2"
                                  :preferred_label "concept v2"}]}]}}
                  body)))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-event-types
  (let [_ (db/create-version-0)
        [_ _ {id :concept/id}] (concepts/assert-concept
                                userid
                                {:type "skill"
                                 :definition "riding bicycle"
                                 :preferred-label "cycling"})
        _ (versions/create-new-version 1)
        _ (concepts/accumulate-concept
           userid
           {:id id
            :preferred-label "riding bicycle"
            :alternative-labels "cycling"})
        _ (daynotes/create-for-concept id userid "Prefer to call it riding")
        _ (versions/create-new-version 2)
        _ (concepts/accumulate-concept
           userid
           {:id id
            :deprecated true})]
    (test/is
     (= [200
         {:data
          {:changelog [{:event "Created"
                        :user "Stallman"
                        :comment ""
                        :concept {:id id
                                  :preferred_label "cycling"
                                  :type "skill"
                                  :alternative_labels []}}
                       {:event "Updated"
                        :user "Stallman"
                        :comment ""
                        :concept {:id id
                                  :type "skill"
                                  :preferred_label "riding bicycle"
                                  :alternative_labels ["cycling"]}
                        :changes [{:attribute "alternative_labels"
                                   :old_value []
                                   :new_value ["cycling"]}
                                  {:new_value "riding bicycle"
                                   :attribute "preferred_label"
                                   :old_value "cycling"}]}
                       {:event "Commented"
                        :user "Stallman"
                        :comment "Prefer to call it riding"
                        :concept {:id id
                                  :type "skill"
                                  :preferred_label "riding bicycle"
                                  :alternative_labels ["cycling"]}}
                       {:event "Deprecated"
                        :user "Stallman"
                        :comment ""
                        :concept {:id id
                                  :type "skill"
                                  :preferred_label "riding bicycle"
                                  :alternative_labels ["cycling"]}}]}}]
        (util/send-request-to-json-service
         :get endpoint
         :headers
         [{:key "api-key"
           :val "222"}]
         :query-params
         [{:key "query"
           :val "{
                     changelog(from:0,to:\"next\") {
                       event: __typename
                       user
                       comment
                       concept {
                         id
                         type
                         preferred_label
                         alternative_labels
                       }
                       ... on Updated {
                         changes {
                           attribute
                           old_value
                           new_value
                         }
                       }
                     }
                   }"}])))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-version-filtering
  (let [_ (db/create-version-0)
        [_ _ {id :concept/id}] (concepts/assert-concept
                                userid
                                {:type "skill"
                                 :definition "riding bicycle"
                                 :preferred-label "cycling"})
        _ (versions/create-new-version 1)
        _ (concepts/accumulate-concept
           userid
           {:id id
            :preferred-label "riding bicycle"
            :alternative-labels "cycling"})
        _ (daynotes/create-for-concept id userid "Prefer to call it riding")
        _ (versions/create-new-version 2)
        _ (concepts/accumulate-concept
           userid
           {:id id
            :deprecated true})]
    (test/is
     (= [200
         {:data
          {:changelog [{:event "Updated"
                        :concept {:id id}}
                       {:event "Commented"
                        :concept {:id id}}]}}]
        (util/send-request-to-json-service
         :get endpoint
         :query-params
         [{:key "query"
           :val "{
                     changelog(from:1,to:2) {
                       event: __typename
                       concept {
                         id
                       }
                     }
                   }"}])))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-relations
  (let [_ (db/create-version-0)
        [_ _ {horse-id :concept/id}]
        (concepts/assert-concept
         userid
         {:type "skill"
          :definition "riding horse"
          :preferred-label "riding horse"})
        [_ _ {car-id :concept/id}]
        (concepts/assert-concept
         userid
         {:type "skill"
          :definition "driving car"
          :preferred-label "driving car"})
        _ (versions/create-new-version 1)
        ;; 1. make 2 concepts related
        _ (concepts/assert-relation userid "riding and driving are related"
                                    horse-id car-id "related"
                                    nil nil)
        ;; 2. replace one with another
        _ (db.core/replace-deprecated-concept
           userid
           horse-id
           car-id
           "Horses are so 1890")
        _ (versions/create-new-version 2)]
    (test/is
      ;; every relation assertion creates 2 events (one for each concept) that
      ;; don't have any defined order, hence using sets:
     (= #{;; 1. make 2 concepts related
          {:event "Updated"
           :concept {:id horse-id}
           :changes [{:attribute "related"
                      :old_value []
                      :new_value [{:id car-id}]}]}
          {:event "Updated"
           :concept {:id car-id}
           :changes [{:attribute "related"
                      :old_value []
                      :new_value [{:id horse-id}]}]}
           ;; 2. replace one with another
          {:event "Deprecated"
           :concept {:id horse-id}}
          {:event "Updated"
           :concept {:id horse-id}
           :changes [{:attribute "replaced_by"
                      :old_value []
                      :new_value [{:id car-id}]}]}
          {:event "Updated"
           :concept {:id car-id}
           :changes [{:attribute "replaces"
                      :old_value []
                      :new_value [{:id horse-id}]}]}}
        (-> (util/send-request-to-json-service
             :get endpoint
             :query-params
             [{:key "query"
               :val "{
                     changelog(from:1,to:2) {
                       event: __typename
                       concept {
                         id
                       }
                       ... on Updated {
                         changes {
                           attribute
                           old_value
                           new_value
                         }
                       }
                     }
                   }"}])
            second
            :data
            :changelog
            set)))))

