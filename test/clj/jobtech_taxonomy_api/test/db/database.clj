(ns jobtech-taxonomy-api.test.db.database
  (:gen-class)
  (:require
   [datahike.api :as dh]
   [datomic.client.api :as d]
   [jobtech-taxonomy-api.config :as config]
   [jobtech-taxonomy-api.db.database-connection :as db]
   [jobtech-taxonomy-api.db.versions :as versions]
   [jobtech-taxonomy-common.db-schema :as db-schema]
   [sendandi.api :as w]
   [taoensso.timbre :as timbre]))

(def isDatahike (= config/db-backend :datahike))

(def ^:private ^{:arglists '([config])} get-conn-with-config
  "create connection unless already created"
  (memoize
   (fn [config]
     (d/connect (d/client config) {:db-name (:datomic-name config)}))))

(defn create-version-0 []
  (when (empty? (versions/get-all-versions))
    (db/transact (db/get-conn)
                 {:tx-data [{:taxonomy-version/id 0
                             :taxonomy-version/tx "datomic.tx"}]})))

(defn- init-datahike [{:keys [path datahike-cfg]}]
  (timbre/set-level! :debug) ;; only set to debug to have a clearer log on testing
  (dh/create-database (assoc datahike-cfg :initial-tx db-schema/schema)))

(defn- init-datomic [{:keys [datomic-cfg]}]
  (let [name (:datomic-name datomic-cfg)]
    (-> (d/client datomic-cfg)
        (d/create-database {:db-name name}))

    ;; The purpose of loop below is to perform repeated attempts to
    ;; initialise the newly created database. It will succeed as soon as the
    ;; database engine is ready creating the database.
    (loop [acc 0]
      (cond
        (>= acc 30) (throw (Exception. "Database cannot be initialised"))
        :else (if (= "DATABASE-DOWN"
                     (try
                       (let [conn (get-conn-with-config datomic-cfg)]
                         (d/transact conn {:tx-data db-schema/schema}))
                       (Thread/sleep 2000)
                       "DATABASE-UP"
                       (catch Exception e "DATABASE-DOWN")))
                (recur (+ acc 1))
                1)))))

(defn init-database [config]
  (if isDatahike
    (init-datahike config)
    (init-datomic config)))

(defn- delete-datahike [{:keys [path datahike-cfg] :as config}]
  (timbre/set-level! :info)
  (dh/delete-database datahike-cfg))

(defn- delete-datomic [{:keys [datomic-cfg] :as config}]
  (let [name (:datomic-name datomic-cfg)]
    (-> (d/client datomic-cfg)
        (d/delete-database {:db-name name}))))

(defn delete-database [config]
  (if isDatahike
    (delete-datahike config)
    (delete-datomic config)))
