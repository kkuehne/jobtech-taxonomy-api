(ns ^:integration-versions-tests jobtech-taxonomy-api.test.versions-test
  (:require [clojure.test :as test :refer [deftest is]]
            [jobtech-taxonomy-api.db.database-connection :as dc]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.test.db.database :as db]
            [jobtech-taxonomy-api.test.test-utils :as util])
  (:import [java.time.temporal ChronoUnit]
           [java.util Date]))

(test/use-fixtures :each util/fixture)

(deftest create-version-test
  (db/create-version-0)
  (is (= 0 (versions/get-current-version-id (dc/get-db)))))

(deftest incremented-versions-only
  (db/create-version-0)
  (is (= 1 (:version (versions/create-new-version 1))))
  (is (= 2 (:version (versions/create-new-version 2))))
  (is (= ::versions/incorrect-new-version (versions/create-new-version 10))))

(deftest rollbacks-are-not-allowed
  (let [_ (db/create-version-0)
        {:keys [version ^Date timestamp]} (versions/create-new-version 1)
        _ (is (= 1 version))
        time-before-release (-> timestamp
                                .toInstant
                                (.minus 1 ChronoUnit/MILLIS)
                                Date/from)]
    (is (= ::versions/new-version-before-release (versions/create-new-version 2 time-before-release)))))
