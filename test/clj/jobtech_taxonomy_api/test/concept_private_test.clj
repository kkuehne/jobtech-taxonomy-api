(ns ^:integration-concepts-tests jobtech-taxonomy-api.test.concept-private-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.versions :as versions]
   [jobtech-taxonomy-api.test.db.database :as db]
   [jobtech-taxonomy-api.test.test-utils :as util]
   [taxonomy :as taxonomy]))

(test/use-fixtures :each util/fixture)

(deftest ^:integration-concept-private-test-0 concepts-private-test-0
  (testing "test assert concept"
    (let [label (gensym "potatisodling-")
          [status body]
          (util/create-new-concept
           {:type "skill"
            :definition label
            :preferred-label label
            :alternative-labels "a | b | c "
            :hidden-labels " a|"})

          id (:concept/id (:concept body))
          created-concept (first (concepts/find-concepts {:id id :version :next}))
          _ (util/update-concept
             {:id id
              :hidden-labels "new-hidden-label-1"
              :alternative-labels "new-alternative-label-1|new-alternative-label-2"})
          updated-concept (first (concepts/find-concepts {:id id :version :next}))]
      (is (= ["a" "b" "c" "new-alternative-label-1" "new-alternative-label-2"]
             (:concept/alternative-labels updated-concept)))
      (is (= ["a" "new-hidden-label-1"]
             (:concept/hidden-labels updated-concept))))))

(deftest ^:integration-private-concept-changes concepts-private-test-1
  (testing "test /v1/taxonomy/private/concept/changes"
    (let [_ (db/create-version-0)
          [status1 body1]
          (util/create-new-concept
           {:type "skill"
            :definition "skulptör"
            :preferred-label "skulptör"
            :alternative-labels "alt1 | alt2"
            :hidden-labels "a1 | a2"})
          createdConcept (-> body1
                             :concept
                             taxonomy/map->nsmap
                             (update :taxonomy/alternative-labels set)
                             (update :taxonomy/hidden-labels set))
          _ (versions/create-new-version 1)
          [status2 body2]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/private/concept/changes"
           :headers [(util/header-auth-admin)]
           :query-params [{:key "after-version", :val "0"}])
          newConcept (-> body2
                         first
                         :taxonomy/new-concept
                         (update :taxonomy/alternative-labels set)
                         (update :taxonomy/hidden-labels set))
          latestConcept (-> body2
                            first
                            :taxonomy/latest-version-of-concept
                            (update :taxonomy/alternative-labels set)
                            (update :taxonomy/hidden-labels set))
          [status3 body3]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/private/concept/changes"
           :headers [(util/header-auth-admin)]
           :query-params [{:key "after-version", :val "1"}])]
      (is (= createdConcept newConcept))
      (is (= newConcept latestConcept))
      (is (= '[] body3))
      (is (= 200 status1))
      (is (= 200 status2))
      (is (= 200 status3)))))

(deftest ^:integration-private-concept-changes concepts-private-test-2
  (testing "test /v1/taxonomy/private/concept/changes"
    (let [_ (db/create-version-0)
          [status1 body1]
          (util/create-new-concept
           {:type "skill"
            :definition "skulptör"
            :preferred-label "skulptör"
            :alternative-labels "alt1 | alt2"
            :hidden-labels "a1 | a2"})
          createdConcept (-> body1
                             :concept
                             taxonomy/map->nsmap
                             (update :taxonomy/alternative-labels set)
                             (update :taxonomy/hidden-labels set))
          [status2 body2]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/private/concept/changes"
           :headers [(util/header-auth-admin)]
           :query-params [{:key "after-version", :val "0"}])
          newConcept (-> body2
                         first
                         :taxonomy/new-concept
                         (update :taxonomy/alternative-labels set)
                         (update :taxonomy/hidden-labels set))
          latestConcept (-> body2
                            first
                            :taxonomy/latest-version-of-concept
                            (update :taxonomy/alternative-labels set)
                            (update :taxonomy/hidden-labels set))]
      (is (= createdConcept newConcept))
      (is (= newConcept latestConcept))
      (is (= 200 status1))
      (is (= 200 status2)))))

(deftest ^:integration-private-concept-changes concepts-private-test-3
  (testing "test one hidden and alt label, /v1/taxonomy/private/concept/changes"
    (let [_ (db/create-version-0)
          [status1 body1]
          (util/create-new-concept
           {:type "skill"
            :definition "skulptör"
            :preferred-label "skulptör"
            :alternative-labels "alt"
            :hidden-labels "hidden"})
          createdConcept (taxonomy/map->nsmap (:concept body1))
          [status2 body2]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/private/concept/changes"
           :headers [(util/header-auth-admin)]
           :query-params [{:key "after-version", :val "0"}])
          newConcept (:taxonomy/new-concept (first body2))
          latestConcept (:taxonomy/latest-version-of-concept (first body2))]
      (is (= createdConcept newConcept))
      (is (= newConcept latestConcept))
      (is (= 200 status1))
      (is (= 200 status2)))))
