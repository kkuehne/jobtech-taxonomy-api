(ns ^:integration-search-tests jobtech-taxonomy-api.test.search-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [jobtech-taxonomy-api.db.versions :as versions]))

(test/use-fixtures :each util/fixture)

(deftest ^:integration-search-test-0 search-test-0
  (testing "test search"
    (let [_ (versions/create-new-version 0)
          [_ _ {:keys [concept/id]}] (concepts/assert-concept
                                      "Stallman"
                                      {:type "skill"
                                       :definition "cyklade"
                                       :preferred-label "cykla"})
          _ (versions/create-new-version 1)
          [status body] (util/send-request-to-json-service
                         :get "/v1/taxonomy/suggesters/autocomplete"
                         :query-params [{:key "query-string", :val "cykla"}])]
      (is (= status 200))
      (is (= body [{:taxonomy/id id
                    :taxonomy/preferred-label "cykla"
                    :taxonomy/type "skill"}])))))

(deftest search-test-1
  (testing "type test"
    (let [_ (versions/create-new-version 0)
          [_ _ {occupation-id :concept/id}]
          (concepts/assert-concept
           ""
           {:type "occupation-field"
            :definition "Försäljning, inköp, marknadsföring"
            :preferred-label "Försäljning, inköp, marknadsföring"})
          [_ _ {ssyk-id :concept/id}]
          (concepts/assert-concept
           ""
           {:type "ssyk-level-3"
            :definition "Förmedlare m.fl."
            :preferred-label "Förmedlare m.fl."})
          _ (versions/create-new-version 1)
          [all-status all-body]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/suggesters/autocomplete"
           :query-params [{:key "query-string", :val "f"}])
          [occupation-status occupation-body]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/suggesters/autocomplete"
           :query-params [{:key "query-string", :val "f"}
                          {:key "type" :val "occupation-field"}])

          [ssyk-status ssyk-body]
          (util/send-request-to-json-service
           :get "/v1/taxonomy/suggesters/autocomplete"
           :query-params [{:key "query-string", :val "f"}
                          {:key "type" :val "ssyk-level-3"}])]
      (testing "no type returns all matches"
        (is (= 200 all-status))
        (is (= #{{:taxonomy/id occupation-id
                  :taxonomy/preferred-label "Försäljning, inköp, marknadsföring"
                  :taxonomy/type "occupation-field"}
                 {:taxonomy/id ssyk-id
                  :taxonomy/preferred-label "Förmedlare m.fl."
                  :taxonomy/type "ssyk-level-3"}}
               (set all-body))))
      (testing "provided type restricts results to the type"
        (is (= 200 occupation-status))
        (is (= #{{:taxonomy/id occupation-id
                  :taxonomy/preferred-label "Försäljning, inköp, marknadsföring"
                  :taxonomy/type "occupation-field"}}
               (set occupation-body)))
        (is (= 200 ssyk-status))
        (is (= #{{:taxonomy/id ssyk-id
                  :taxonomy/preferred-label "Förmedlare m.fl."
                  :taxonomy/type "ssyk-level-3"}}
               (set ssyk-body)))))))

