(ns jobtech-taxonomy-api.test.relation-private-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.versions :as versions]
   [jobtech-taxonomy-api.test.db.database :as db]
   [jobtech-taxonomy-api.test.test-utils :as util]
   [jobtech-taxonomy-common.relation :as relation]
   [taxonomy :as taxonomy]))

(test/use-fixtures :each util/fixture)

(deftest ^:private-relation private-relation-0
  (testing "test asserting a relation"
    (let [relation "substitutability"
          [_ bd1]
          (util/create-new-concept
           {:type "skill"
            :definition "Haskell, programmeringsspråk"
            :preferred-label "Haskell, programmeringsspråk"})
          id1 (:concept/id (:concept bd1))
          [_ bd2]
          (util/create-new-concept
           {:type "esco-skill"
            :definition "Haskell"
            :preferred-label "Haskell"})
          id2 (:concept/id (:concept bd2))
          [status1 body1]
          (util/create-new-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "100"})
          [status2 body2] ;; fail to create the same relation again
          (util/create-new-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "100"})
          [status3 body3] ;; fail to update the relation without a comment
          (util/update-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "50"})
          [status4 body4]
          (util/update-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "50"
            :comment "Update percentage"})]
      (is (= 200 status1))
      (is (= 409 status2))
      (is (= 400 status3))
      (is (= 200 status4)))))
