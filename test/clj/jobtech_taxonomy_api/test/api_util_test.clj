(ns jobtech-taxonomy-api.test.api-util-test
  (:require
   [clojure.test :as test]
   [jobtech-taxonomy-api.db.api-util :as api-util]))

(test/deftest test-matchers
  (test/testing "Test match anywhere"
    (test/is (.matches "mekanoid" (api-util/str-to-pattern-contains "noi")))
    (test/is (not (.matches "mekanoid" (api-util/str-to-pattern-contains "anoiM")))))

  (test/testing "Test match ignore case"
    (test/is (.matches "Mekanoid" (api-util/str-to-pattern-ignore-case "mekanoid")))
    (test/is (not (.matches "Mekanoid" (api-util/str-to-pattern-ignore-case "anoid")))))

  (test/testing "Test match starts with"
    (test/is (.matches "Mekanoid" (api-util/str-to-pattern-starts-with "mek")))
    (test/is (not (.matches "mekanoid" (api-util/str-to-pattern-starts-with "oid"))))))
