(ns ^:integration-daynotes-tests jobtech-taxonomy-api.test.daynotes-test
  (:require [clojure.test :as test :refer [deftest is]]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.daynotes :as daynotes]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [taxonomy :as taxonomy])
  (:import [java.time.temporal ChronoUnit]
           [java.util Date]))

(test/use-fixtures :each util/fixture)

(defn create-next-version! []
  (let [current-version (versions/get-current-version-id (db/get-db))]
    (if (nil? current-version)
      (do (db/transact (db/get-conn)   {:tx-data [{:taxonomy-version/id 0
                                                   :taxonomy-version/tx "datomic.tx"}]})
          0)
      (let [next-version (inc current-version)]
        (do
          (versions/create-new-version next-version)
          next-version)))))

(defn create-concept! []
  (let [concept-pl (str (gensym "cykla-"))]
    (last (concepts/assert-concept "Stallman" {:type "skill"
                                               :definition concept-pl
                                               :preferred-label concept-pl}))))

(defn create-concept-and-version! []
  (let [version (create-next-version!)
        new-concept (create-concept!)
        next-version (create-next-version!)]
    [version next-version new-concept]))

(defn call-relation-changes-service
  ([after-version]
   (call-relation-changes-service after-version nil))
  ([after-version to-version-inclusive]
   {:pre [(int? after-version)  (or nil? (int? to-version-inclusive))]}
   (second (util/send-request-to-json-service
            :get "/v1/taxonomy/main/relation/changes"
            :query-params [{:key "after-version", :val after-version}
                           (when to-version-inclusive {:key "to-version-inclusive" :val to-version-inclusive})]))))

(defn call-private-relation-changes-service [after-version]
  {:pre [(int? after-version)]}
  (second (util/send-request-to-json-service
           :get "/v1/taxonomy/private/relation/changes"
           :headers [(util/header-auth-admin)]
           :query-params [{:key "after-version", :val after-version}])))

(defn get-latest-version-id-part []
  (-> (util/send-request-to-json-service
       :get "/v1/taxonomy/main/versions")
      second))

(defn get-latest-version-id []
  (-> (get-latest-version-id-part)
      last
      :taxonomy/version
      (or 0)))

(defn get-next-to-latest-version-id []
  (-> (get-latest-version-id-part)
      butlast
      last
      :taxonomy/version
      (or 0)))

(defn get-latest-change-from-changes-service []
  (last (call-relation-changes-service (get-next-to-latest-version-id))))

(defn get-latest-change-from-private-changes-service []
  (last (call-private-relation-changes-service (get-latest-version-id))))

(defn change-ns-and-strip-keys [concept]
  (dissoc  (taxonomy/map->nsmap  concept) :taxonomy/definition))

(defn create-new-concepts-and-a-relation! []
  (let [version   (create-next-version!)
        concept-1 (create-concept!)
        concept-2 (create-concept!)
        relation  (last (concepts/assert-relation "Stallman" nil (:concept/id concept-1) (:concept/id concept-2)  "substitutability" nil 50))]

    [relation concept-1 concept-2]))

(deftest ^:integration-daynotes-test-changes-of-created-concept test-changes-of-created-concept
  (let [[_ _ concept] (create-concept-and-version!)
        [create-event] (daynotes/get-for-concept (:concept/id concept) nil nil nil nil true)
        time-between-events (-> create-event
                                ^Date (:timestamp)
                                .toInstant
                                (.plus 1 ChronoUnit/MILLIS)
                                Date/from)
        _ (is (= "CREATED" (:event-type create-event)))
        _ (is (= concept (:new-concept create-event)))
        new-definition (str (gensym "Reading-"))
        _ (concepts/accumulate-concept "Stallman" {:id (:concept/id concept) :definition new-definition})
        update-event (second (daynotes/get-for-concept (:concept/id concept) nil nil nil nil true))
        _ (is (= [{:attribute "definition"
                   :old-value (:concept/definition concept)
                   :new-value new-definition
                   :added [new-definition]
                   :removed [(:concept/definition concept)]}]
                 (:concept-attribute-changes update-event)))]

    (is (= [update-event]
           (daynotes/get-for-concept (:concept/id concept) time-between-events nil nil nil true)))))

(deftest ^:integration-daynotes-test-relation-changes  test-relations-changes
  (let [[relation concept1 concept2] (create-new-concepts-and-a-relation!)
        [related-event] (daynotes/get-for-relation (:concept/id concept1) nil nil nil nil true)
        _ (is (= "CREATED" (:event-type related-event)))
        _ (is (= (:concept/id concept1) (-> related-event :relation :source :concept/id)))
        _ (is (= (:concept/id concept2) (-> related-event :relation :target :concept/id)))
        time-between-events (-> related-event
                                ^Date (:timestamp)
                                .toInstant
                                (.plus 1 ChronoUnit/MILLIS)
                                Date/from)
        _ (concepts/assert-relation "Stallman" nil (:concept/id concept2) (:concept/id concept1)
                                    "broader" "Writing requires reading and more" nil)

        broader-event (second (daynotes/get-for-relation (:concept/id concept2) nil nil nil nil true))
        daynote-concept-2 (daynotes/get-for-relation (:concept/id concept2) time-between-events nil nil nil true)]

    (is (= [broader-event]
           (daynotes/get-for-relation (:concept/id concept2) time-between-events nil nil nil true)))
    (is (= [related-event]
           (daynotes/get-for-relation (:concept/id concept2) nil time-between-events nil nil true)))))

(test/deftest ^:integration-daynotes-test-1 test-relation-changes-unpublished

  (let [[relation concept-1 concept-2] (create-new-concepts-and-a-relation!)
        relation-change                (get-latest-change-from-private-changes-service)

        event-relation-type (get-in relation-change [:taxonomy/relation :taxonomy/relation-type])
        event-concept-1     (get-in relation-change [:taxonomy/relation :taxonomy/source])
        event-concept-2     (get-in relation-change [:taxonomy/relation :taxonomy/target])]

    (is (= event-concept-1 (change-ns-and-strip-keys concept-1)))
    (is (= event-concept-2 (change-ns-and-strip-keys concept-2)))
    (is (= event-relation-type (:relation/type relation)))))

(test/deftest ^:integration-daynotes-test-2 test-relation-changes-published

  (let [[relation concept-1 concept-2] (create-new-concepts-and-a-relation!)
        next-version                   (create-next-version!)
        relation-change                (get-latest-change-from-changes-service)

        event-relation-type (get-in relation-change [:taxonomy/relation :taxonomy/relation-type])
        event-concept-1     (get-in relation-change [:taxonomy/relation :taxonomy/source])
        event-concept-2     (get-in relation-change [:taxonomy/relation :taxonomy/target])]

    (is (= event-concept-1 (change-ns-and-strip-keys concept-1)))
    (is (= event-concept-2 (change-ns-and-strip-keys concept-2)))
    (is (= event-relation-type (:relation/type relation)))))
