(ns jobtech-taxonomy-api.test.test-utils
  (:require
   [clojure.pprint :as pp]
   [clojure.string :as str]
   [clojure.test :as test]
   [jobtech-taxonomy-api.authentication-service :as auth-service]
   [jobtech-taxonomy-api.config :as config]
   [jobtech-taxonomy-api.db.database-connection :as conn]
   [jobtech-taxonomy-api.handler :as handler]
   [jobtech-taxonomy-api.middleware.formats :as formats]
   [jobtech-taxonomy-api.test.db.database :as db]
   [mount.core :as mount]
   [muuntaja.core :as m]
   [ring.mock.request :as mock])
  (:import (java.util UUID)))

(defn header-auth-admin [] {:key "api-key", :val (auth-service/get-token :admin)})

(defn fixture
  "Setup a temporary database, run (f), and then teardown the database."
  [f]
  (let [config (update-in (config/make-config) [:datomic-cfg :datomic-name] str "kaocha-test-" (UUID/randomUUID))]
    (try
      (db/init-database config)

      (println (mount/start-with-states
                {#'config/env {:start (fn [] config)}}))

      (println (mount/start
                #'conn/conn
                #'handler/app))

      (f)

      (println (mount/stop #'conn/conn
                           #'handler/app))
      (println (mount/stop))

      (finally
        (db/delete-database config)))))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(defn make-req [req [first & rest]]
  (if (nil? first)
    req
    (make-req (mock/header req (get first :key) (get first :val)) rest)))

(defn make-request [method endpoint & {:keys [headers query-params]}]
  (let [req (mock/request method endpoint)
        req-w-headers (if headers
                        (make-req req headers)
                        req)]
    (if query-params
      (mock/query-string req-w-headers
                         (str/join "&" (map #(str/join "=" (list (get % :key) (get % :val))) query-params)))
      req-w-headers)))

(defn send-request [method endpoint & {:keys [headers query-params]}]
  (let [req (make-request method endpoint :headers headers, :query-params query-params)]
    (handler/app req)))

(defn send-request-to-json-service [method endpoint & {:keys [headers query-params]}]
  (let [response (send-request method endpoint :headers headers, :query-params query-params)
        status (:status response)
        body (parse-json (:body response))]
    (list status body)))

(defn- map2query-params [m]
  (mapv (fn [[k v]] {:key (name k) :val v}) m))

(defn create-new-concept [params]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/concept"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn update-concept [params]
  (send-request-to-json-service
   :patch "/v1/taxonomy/private/accumulate-concept"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn create-new-relation [params]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/relation"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn update-relation [params]
  (send-request-to-json-service
   :patch "/v1/taxonomy/private/accumulate-relation"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn test-var
  "Test a var and print results

  Useful at the REPL

  Note that for integration tests the system should be stopped before testing
  the var"
  [var]
  (let [report test/report]
    (binding [test/*test-out* *out*
              test/*report-counters* (ref test/*initial-report-counters*)
              test/report (fn [m]
                            (case (:type m)
                              :fail
                              (test/with-test-out
                                (test/inc-report-counter :fail)
                                (println "\nFAIL in" (test/testing-vars-str m))
                                (when (seq test/*testing-contexts*) (println (test/testing-contexts-str)))
                                (when-let [message (:message m)] (println message))
                                (println "expected:")
                                (pp/pprint (:expected m))
                                (println "  actual:")
                                (pp/pprint (:actual m)))
                              (report m)))]
      (test/test-vars [var])
      (test/report (assoc @test/*report-counters* :type :summary)))))
