(ns ^:integration-webhooks-tests jobtech-taxonomy-api.test.webhooks-test
  (:require [clojure.core.async :refer [<!!]]
            [clojure.test :as test]
            [jobtech-taxonomy-api.webhooks :as webhooks]))

(test/deftest ^:webhook-test-0 test-webhooks-0
  (test/testing "Test using single webhook"
    (let [result (<!! (webhooks/notify! {:url "https://postman-echo.com/post" :headers {}} 1))]
      (test/is (= (:status result) 200))))
  (test/testing "Webhook deduplication"
    (let [ch-1 (webhooks/notify! {:url "https://postman-echo.com/post" :headers {}} 1)
          ch-2 (webhooks/notify! {:url "https://postman-echo.com/post" :headers {}} 1)]
      (test/is (identical? (<!! ch-1) (<!! ch-2))))))
