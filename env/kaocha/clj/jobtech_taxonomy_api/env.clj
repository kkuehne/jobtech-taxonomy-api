(ns jobtech-taxonomy-api.env
  (:require [clojure.tools.logging :as log]
            [selmer.parser :as parser]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[jobtech-taxonomy-api started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[jobtech-taxonomy-api has shut down successfully]=-"))
   :middleware identity})
