(ns user
  (:require [clojure.spec.alpha :as s]
            [expound.alpha :as expound]
            [jobtech-taxonomy-api.config :refer [env]]
            jobtech-taxonomy-api.core
            [mount.core :as mount]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(defn start []
  (mount/start-without #'jobtech-taxonomy-api.core/repl-server))

(defn stop []
  (mount/stop-except #'jobtech-taxonomy-api.core/repl-server))

(defn restart []
  (stop)
  (start))

(comment
  (stop)
  (reset! @#'mount/-args {:options {:port 3000}})
  (start)
  (restart)

  ,)